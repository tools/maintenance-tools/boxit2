import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import BoxIt 1.0

import "./Utils.js" as Utils

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: "BoxIt2"
    id: mainWindow

    property var selected: []

    property UploadQueue queue: FsService.startUpload()

    header: ToolBar{
        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.name: "draw-arrow-back"
                enabled: !popup.visible
                onClicked: {
                    currentFolders.pop();
                    currentFoldersChanged();
                }
            }
            Pane{
                Layout.fillWidth: true
                padding: 0
                background: Rectangle{
                    radius: 4
                    color: systemPalette.alternateBase
                    border.color: systemPalette.mid
                    clip: true
                }
                RowLayout{
                    enabled: !popup.visible

                    spacing: 0
                    anchors.top:parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    clip: true
                    Repeater {
                        model: currentFolders
                        Label{
                            Layout.fillWidth: false
                            clip: true
                            background: Rectangle{
                                color:
                                    pathLabelMouseArea.containsPress
                                    ?Utils.withAlpha(systemPalette.highlight,0.5)
                                    :pathLabelMouseArea.containsMouse?Utils.withAlpha(systemPalette.dark,0.5):"transparent"
                            }

                            MouseArea{
                                id: pathLabelMouseArea
                                hoverEnabled: true
                                anchors.fill: parent
                                onClicked: {
                                    currentFolders.splice(index+1);
                                    currentFoldersChanged();
                                }
                            }
                            padding: 5
                            text: modelData+" /"
                        }
                    }
                }
            }
            ToolButton {
                icon.name: "view-refresh"
                onClicked: {
                    currentFoldersChanged();
                }
            }
        }
    }

    property var currentFolders: [""]

    onCurrentFoldersChanged: {
        if(currentFolders.length===0){
            currentFolders.push("");
            return;
        }
        if(currentFolders.length===3){
            queue.branch = currentFolders[1];
            queue.repo = currentFolders[2];
        }
    }

    SystemPalette {
        id: systemPalette
    }

    Connections{
        target: FsService
        function onProgressChanged(value){
            if(value===100){
                currentFoldersChanged();
            }
        }
    }

    ProgressPopup {
        height: 85
        width: parent.width/3

        visible: queue.size>0 || FsService.progress!==100

        id: popup
        focus: true

        pendings: queue.entries
        headerText: FsService.currentFileName
        progress: FsService.progress

        enter: Transition {
            NumberAnimation{
                easing.type: Easing.InCubic
                property: "y"
                from: mainWindow.height
                to:(mainWindow.height-popup.height)-50
            }

        }
        exit: Transition {
            NumberAnimation{
                property: "y"
                easing.type: Easing.OutCubic
                to: mainWindow.height
                from:(mainWindow.height-popup.height)-50
            }

        }
    }

    Pane{
        padding: 0
        z:0
        background: Rectangle {
            color: systemPalette.alternateBase
        }
        anchors.fill: parent

        PackageDropArea {
            id: dragArea
            anchors.fill: parent
            z: 100
            enabled: currentFolders.length==3
            onDropped: {
                acceptedDrag = undefined
                let currentPendings = [];

                for(var i = 0; i < drop.urls.length; i++) {
                    if(drop.urls[i].endsWith(".pkg.tar.zst") || drop.urls[i].endsWith(".pkg.tar.zst.sig")) {
                        currentPendings.push(drop.urls[i])
                    }
                }

                if(currentPendings.length === 0) {
                    drop.accepted = false;
                    return false;
                }
                for(let pending of currentPendings){
                    if(pending.endsWith(".sig")){
                        queue.addSignature(pending)
                    } else {
                        queue.addPackage(pending)
                    }
                }
            }
        }

        RubberbandContainer {
            id: rubberbandContainer
            anchors.fill: parent
            z: 0

            onWheel: {
                if(fileGrid.contentHeight<fileGrid.height){
                    return;
                }

                if(fileGrid.contentY-wheel.angleDelta.y < 0){
                    fileGrid.contentY = 0;
                    return;
                }
                if(fileGrid.contentY+fileGrid.height-wheel.angleDelta.y > fileGrid.contentHeight){
                    fileGrid.contentY = fileGrid.contentHeight-fileGrid.height;
                    return;
                }

                fileGrid.contentY-=wheel.angleDelta.y;
            }
        }

        GridView {
            clip: true

            ScrollBar.vertical: mainScroll
            id: fileGrid
            boundsBehavior: Flickable.StopAtBounds
            anchors.centerIn: parent
            height: parent.height
            width: parent.width-25
            model: FsService.parsePath(currentFolders)
            cellHeight: 100
            cellWidth: 120
            interactive: false
            z:5
            delegate: ToolButton {
                z:10
                checkable: true
                checked: selected.includes(modelData)
                height: 104
                width: 114
                down: area.containsPress
                ToolTip.visible: area.containsMouse
                ToolTip.delay: 600
                ToolTip.text: modelData
                ColumnLayout{

                    anchors.margins: 5
                    anchors.fill: parent
                    Image {
                        Layout.preferredHeight: 64
                        Layout.preferredWidth: 64
                        Layout.alignment: Qt.AlignHCenter
                        id: name
                        sourceSize.height: 64
                        sourceSize.width: 64
                        source: {
                            if(currentFolders.length==3){
                                if(modelData.endsWith(".sig")){
                                    return "file:///usr/share/icons/breeze/mimetypes/64/application-pgp-signature.svg";
                                } else {
                                    return "file:///usr/share/icons/breeze/mimetypes/64/package-x-generic.svg";
                                }
                            }

                            return "file:///usr/share/icons/breeze/places/64/folder.svg"
                        }
                    }
                    Label{
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        elide: Text.ElideRight
                        text: modelData
                        font.pointSize: 8
                        maximumLineCount: 2
                    }
                }
                MouseArea{
                    hoverEnabled: true
                    id: area
                    z:30
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked: {

                        if(currentFolders.length>=3 ){
                            if(mouse.button == Qt.RightButton){
                                contextMenu.open();
                            } else if (!selected.includes(modelData)) {
                                selected.push(modelData);
                                selectedChanged();
                            } else {
                                selected = selected.filter(item => item !== modelData);
                            }


                            return;
                        }
                        selected = {};

                        currentFolders.push(modelData);
                        currentFoldersChanged();

                    }
                }
                Menu {
                    width: parent.width
                    y: parent.height-height
                    id: contextMenu

                    MenuItem {
                        text: "Remove"
                        onTriggered: {
                            let v = FsService.removeFile(currentFolders[1],
                                                         currentFolders[2],
                                                         modelData);
                            if(v){
                                currentFoldersChanged();
                            }
                        }
                    }
                }

            }
        }

    }
    ScrollBar {
        id: mainScroll
        anchors.right: parent.right
        anchors.top:parent.top
        anchors.bottom: parent.bottom
    }
}
