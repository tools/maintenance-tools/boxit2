import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import "./Utils.js" as Utils

MouseArea{

    property var originalCoords: ({})
    id: rubberbandContainer


    onPressedChanged:  {
        if(pressed && Object.keys(originalCoords).length==0){
            selected = []
            originalCoords = {mouseX, mouseY};
        } else if(!pressed && Object.keys(originalCoords).length!=0){
            for(let i = rubberband.x+120/2;
                i < rubberband.width+rubberband.x-120/2;
                i++){
                for(let j = rubberband.y+100/2;
                    j < rubberband.height+rubberband.y-100/2;
                    j++){
                    let index = fileGrid.indexAt(i+fileGrid.contentX,j+fileGrid.contentY);
                    if(index>0 && !selected.includes(fileGrid.model[index])){
                        selected.push(fileGrid.model[index])
                        selectedChanged();
                    }
                }
            }
            originalCoords = {};
        }
    }
    Rectangle {
        Behavior on opacity {
            NumberAnimation{duration: 100}
        }
        id:rubberband
        opacity: Object.keys(rubberbandContainer.originalCoords).length!=0

        x:Math.min(rubberbandContainer.originalCoords.mouseX, Math.max(rubberbandContainer.mouseX,0))
        y:Math.min(rubberbandContainer.originalCoords.mouseY, Math.max(rubberbandContainer.mouseY,0))
        width: Math.abs(rubberbandContainer.originalCoords.mouseX-Math.max(rubberbandContainer.mouseX,0))
        height:Math.abs(rubberbandContainer.originalCoords.mouseY-Math.max(rubberbandContainer.mouseY,0))
        color: Utils.withAlpha(systemPalette.highlight, .5)
        border.color: systemPalette.highlight
        radius: 2
    }
}
