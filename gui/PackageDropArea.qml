import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import BoxIt 1.0

DropArea{
    property bool enabled: false
    SystemPalette {
        id: systemPalette
    }


    id:dragArea
    property var acceptedDrag: undefined

    onEntered: {
        if(!enabled){
            drag.accepted = false;
            return;
        }

        for(var i = 0; i < drag.urls.length; i++) {
            if(drag.urls[i].endsWith(".pkg.tar.zst") || drag.urls[i].endsWith(".pkg.tar.zst.sig")) {
                acceptedDrag = true;
                drag.accepted = true;
            } else {
                acceptedDrag = false;
                drag.accepted = false;
            }
            return;
        }
    }

    onExited: {
        acceptedDrag = undefined;
    }

    Rectangle{
        id:visualArea
        anchors.fill: parent
        z: 100

        Connections {
            target: dragArea
            function onAcceptedDragChanged(){
                if(dragArea.acceptedDrag!==undefined){
                    if(dragArea.acceptedDrag){
                        visualArea.color = systemPalette.highlight;
                        visualAreaLabel.color = systemPalette.highlightedText;
                        visualAreaLabel.text = "Drop packages here";
                    } else {
                        visualArea.color = "#e74c3c";
                        visualAreaLabel.color = "white";
                        visualAreaLabel.text = "Not a supported type of package";
                    }
                }
            }
        }

        opacity: dragArea.enabled && dragArea.acceptedDrag!=undefined

        Behavior on opacity {
            NumberAnimation{
                duration: 100
            }
        }

        Label{
            id: visualAreaLabel
            anchors.centerIn: parent
            font.pointSize: 12
            color: systemPalette.highlightedText

        }
    }




}
