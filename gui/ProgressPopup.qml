import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import BoxIt 1.0

Popup {
    property int progress: 100
    property string headerText
    property var pendings: []
    id: popup
    x: (parent.width-width)-10
    y: (parent.height-height)-10
    closePolicy: Popup.NoAutoClose
    padding: 10


    background: Item{

        Rectangle{
            color: systemPalette.alternateBase
            radius: 2
            anchors.fill: parent
            anchors.margins: 8
            id: bgPane
        }
        layer.enabled: true
        layer.effect: DropShadow {
            verticalOffset: -1
            radius: 8.0
            samples: 14
            color: "#80000000"
        }
    }
    ColumnLayout{

        anchors.fill: parent
        Label {
            Layout.fillWidth: true
            id: popupLabel
            text: headerText
        }
        RowLayout{
            Layout.fillWidth: true
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            ProgressBar{
                Layout.fillWidth: true
                id: progressbar
                value: progress
                from:0
                to:100
            }
            Label{
                visible: progress<100
                Layout.preferredWidth: parent.width/10
                text: progress+"%"

            }
        }
        Label {
            visible: pendings.length>0
            Layout.fillWidth: true
            id: pendingLabel
            text: pendings.length + " pending"
        }
    }

}
