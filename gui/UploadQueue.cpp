#include "UploadQueue.h"
#include <QtConcurrent/QtConcurrent>
#include <QFile>
#include <FsService.h>

#include <protos/filesystem.grpc.pb.h>


QString UploadQueue::repo() const
{
    return m_repo;
}

QString UploadQueue::branch() const
{
    return m_branch;
}

void UploadQueue::addPackage(const QUrl &filename)
{
    {
        std::lock_guard<std::mutex> lg(m_lock);
        m_queue.append(QPair(filename,false));
        if(!m_running){
            process();
        }
    }

    Q_EMIT entriesChanged(entries());
    Q_EMIT sizeChanged(m_queue.size());
}

void UploadQueue::addSignature(const QUrl &filename)
{
    {
        std::lock_guard<std::mutex> lg(m_lock);
        m_queue.append(QPair(filename,true));
        if(!m_running){
            process();
        }
    }
    Q_EMIT entriesChanged(entries());
    Q_EMIT sizeChanged(m_queue.size());
}

int UploadQueue::size() const
{
    return m_queue.size();
}

QStringList UploadQueue::entries() const
{
    QStringList lst;
    lst.reserve(m_queue.size());
    std::transform(m_queue.begin(), m_queue.end(), std::back_inserter(lst), [](const auto& pair){return pair.first.fileName();});
    return lst;
}

void UploadQueue::setRepo(QString repo)
{
    if (m_repo == repo)
        return;

    m_repo = repo;
    Q_EMIT repoChanged(m_repo);
}

void UploadQueue::setBranch(QString branch)
{
    if (m_branch == branch)
        return;

    m_branch = branch;
    Q_EMIT branchChanged(m_branch);
}

void UploadQueue::process()
{
    m_running=true;
    std::thread thr([this](){
        while(!m_queue.empty()){
            QUrl file;
            bool isSignature;
            {
                std::lock_guard<std::mutex> lg(m_lock);

                auto pair = m_queue.takeFirst();
                file = pair.first;
                isSignature = pair.second;

            }
            Q_EMIT entriesChanged(entries());
            Q_EMIT sizeChanged(m_queue.size());

            m_service->sendFile(file);
            auto fn = file.fileName();
            if(isSignature){
                fn.chop(4);
            }
            m_service->registerPackage(branch(),repo(), fn);

        }
        m_running = false;
    });
    thr.detach();

}
