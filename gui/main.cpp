#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQuickStyle>
#include <QMessageBox>

#include <fstream>
#include <FsService.h>
#include <UploadQueue.h>

inline std::string load_cert(const std::string& key_cert_file){
    
    auto path = global::settings.get<std::string>("certs-path").value_or(global::default_settings::certificate_path) + "/" + key_cert_file;
    std::ifstream cert_file(path);

    return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    QQuickStyle::setStyle("org.kde.desktop");


    QQmlApplicationEngine engine;
    QQuickWindow::setTextRenderType(QQuickWindow::NativeTextRendering);

    global::credentials_manager.set_backend(
        new credentials_manager_storage_plain_config(global::settings));



    if(!global::credentials_manager.get().has_value() ||
            !global::credentials_manager.get()->password.has_value()){
        QMessageBox errorBox;
        errorBox.setText("Configuration and/or credentials are not found. You should log-in using the CLI utility to use this GUI");
        errorBox.setIcon(QMessageBox::Icon::Critical);
        errorBox.exec();
        return 1;
    }


    const QUrl url(QStringLiteral("qrc:/main.qml"));

    qmlRegisterSingletonType<FsService>("BoxIt", 1, 0, "FsService", [](QQmlEngine *engine,[[maybe_unused]] QJSEngine *scriptEngine) -> QObject* {
        Q_UNUSED(engine)


        auto host = *global::settings.get<std::string>({"profiles", "default", "host"});
#if NO_SSL
        auto node = global::settings.get<YAML::Node>({"profiles", "default"});
        credentials creds;
        if(!node || !(*node)["host"].IsDefined()){
            auto creds = global::credentials_manager.profile_prompt();
        } else {
            creds.host = (*node)["host"].as<std::string>("");
        }
        auto insecure_creds = grpc::InsecureChannelCredentials();
        return new FsService(grpc::CreateChannel(creds.host, insecure_creds), engine);
            
#else
        std::string cert = load_cert("server.crt");

        grpc::SslCredentialsOptions ssl_opts = {.pem_root_certs = cert};

        grpc::ChannelArguments chargs;
        auto target_override = global::settings.get<std::string>("ssl-target");
        if(target_override.has_value()){
            chargs.SetSslTargetNameOverride(target_override.value());
        }

        return new FsService(grpc::CreateCustomChannel(host, grpc::SslCredentials(ssl_opts), chargs), engine);
#endif
    });
    qmlRegisterUncreatableType<UploadQueue>("BoxIt", 1,0, "UploadQueue", "");

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
