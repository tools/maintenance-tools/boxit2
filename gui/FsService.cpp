#include "FsService.h"
#include <string>
#include <vector>
#include <algorithm>
#include <QFile>
#include <QtConcurrent/QtConcurrent>
template <typename TIterator>
QStringList fromStdStringArray(const TIterator& begin, const TIterator& end)
{
    QStringList result;
    result.reserve(end-begin);

    std::transform(begin,end, std::back_inserter(result),
                   [](const std::string& str){return QString::fromStdString(str);});

    return result;
}

QStringList FsService::getRoot()
{
    RootRequest rq;
    RootReply rp;
    auto ctx = create_default_context();

    auto status = m_stub->GetRoot(ctx.get(),rq,&rp);
    if(!status.ok()){
        return {};
    }

    return fromStdStringArray(rp.branches().begin(), rp.branches().end());
}

QStringList FsService::getBranch(const QString &branch)
{
    BranchRequest rq;
    BranchReply rp;
    auto ctx = create_default_context();

    rq.set_name(branch.toStdString());

    auto status = m_stub->GetBranch(ctx.get(), rq, &rp);

    if(!status.ok()){
        return {};
    }

    return fromStdStringArray(rp.repos().begin(), rp.repos().end());
}

QStringList FsService::getRepo(const QString &branch, const QString &repo)
{
    PackagesRequest rq;
    PackagesReply rp;
    auto ctx = create_default_context();

    rq.set_branch(branch.toStdString());
    rq.set_repo(repo.toStdString());

    auto status = m_stub->GetRepoPackages(ctx.get(),rq,&rp);

    if(!status.ok()){
        return {};
    }
    return fromStdStringArray(rp.packages().begin(), rp.packages().end());
}

QStringList FsService::parsePath(const QStringList &folders)
{
    switch (folders.size()) {
    case 1:
        return getRoot();
    case 2:
        return getBranch(folders[1]);
    case 3:
        return getRepo(folders[1],folders[2]);
    default:
        return {};
    }
}

bool FsService::removeFile(const QString &branch, const QString &repo, const QString &pkg)
{
    RemoveRequest rq;

    rq.set_branch(branch.toStdString());
    rq.set_repo(repo.toStdString());
    rq.set_package_name(pkg.toStdString());

    RemoveReply rp;

    auto ctx = create_default_context();

    auto status = m_stub->RemovePackage(ctx.get(), rq, &rp);


    return status.ok();
}

UploadQueue* FsService::startUpload()
{
    return new UploadQueue(this, this);
}


void FsService::sendFile(const QUrl &filename)
{
    setCurrentFileName(filename.fileName());
    QFile file(filename.path());

    if(!file.open(QFile::ReadOnly)){
        return;
    }


    WriteReply repl;
    auto ctx = create_default_context();
    auto writer = m_stub->WriteOverlay(ctx.get(), &repl);
    WriteRequest rq;

    auto md = rq.mutable_metadata();
    md->set_name(filename.fileName().toStdString());
    md->set_offset(0);
    auto st = writer->Write(rq);
    qDebug()<<st;
    uint64_t total = 0;

    while(!file.atEnd()){



        WriteRequest rq1;
        auto block = rq1.mutable_data_block();

        auto bytes = file.read(524'288);


        block->set_data(bytes,bytes.size());
        block->set_size(bytes.size());

        total+=bytes.size();


        auto repl = writer->Write(rq1);
        if(repl){
            int sz = file.size();
            setProgress((total * 100) / sz);

        } else {
            break;
        }

    }

    writer->WritesDone();
    auto status = writer->Finish();
}

bool FsService::registerPackage(const QString &branch, const QString &repo, const QString &pkg)
{
    RegisterRequest rq;
    RegisterReply rp;

    rq.set_branch(branch.toStdString());
    rq.set_repo(repo.toStdString());
    rq.set_package_name(pkg.toStdString());

    auto ctx = create_default_context();

    auto status = m_stub->RegisterPackage(ctx.get(), rq, &rp);

    return status.ok();
}

void FsService::setProgress(int progress)
{
    if (m_progress == progress)
        return;

     qDebug()<<"Upload progress " << progress << "%...";
    m_progress = progress;
    Q_EMIT progressChanged(m_progress);
}
