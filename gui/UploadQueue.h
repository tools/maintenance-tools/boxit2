#pragma once
#include <QObject>
#include <QString>
#include <QQueue>
#include <memory>
#include <mutex>
#include <atomic>

class FsService;

class UploadQueue : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList entries READ entries NOTIFY entriesChanged)
    Q_PROPERTY(int size READ size NOTIFY sizeChanged)

    Q_PROPERTY(QString branch READ branch WRITE setBranch NOTIFY branchChanged)
    Q_PROPERTY(QString repo READ repo WRITE setRepo NOTIFY repoChanged)

public:
    explicit UploadQueue(FsService* service, QObject* parent = nullptr): QObject(parent), m_service(service){}

    QString repo() const;

    QString branch() const;
    
    int size() const;
    
    QStringList entries() const;

public Q_SLOTS:
    void addPackage(const QUrl&filename);
    void addSignature(const QUrl&filename);


    void setRepo(QString repo);

    void setBranch(QString branch);

Q_SIGNALS:
    void entriesChanged(QStringList entries);
    void sizeChanged(int size);

    void repoChanged(QString repo);

    void branchChanged(QString branch);

private:
    void process();
    std::atomic<bool> m_running;
    std::mutex m_lock;
    QQueue<QPair<QUrl, bool>> m_queue;
    FsService* m_service;

    QStringList m_entries;
    int m_size;
    QString m_repo;
    QString m_branch;
};
