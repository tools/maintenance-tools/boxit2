cmake_minimum_required(VERSION 3.14)

project(boxit-gui LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check https://doc.qt.io/qt/deployment-android.html for more information.
# They need to be set before the find_package(../boxit-cli.) calls below.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core Quick QuickControls2 REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Quick QuickControls2 Widgets REQUIRED)
find_package(PkgConfig REQUIRED)
find_package(Protobuf REQUIRED)
find_package(gRPC REQUIRED)
find_package(OpenSSL REQUIRED)

pkg_check_modules(YAML_CPP REQUIRED yaml-cpp)
pkg_check_modules(SECRET REQUIRED libsecret-1 gio-2.0)


set(PROJECT_SOURCES
        main.cpp
        qml.qrc
        FsService.cpp
        FsService.h
        UploadQueue.cpp
        UploadQueue.h
        ../boxit-cli/utils/secret.cpp
        ../boxit-cli/utils/secret.h
        ../boxit-cli/utils/credentials_manager.cpp
        ../boxit-cli/utils/credentials_manager.h
        ../boxit-cli/utils/cli_io.cpp
        ../boxit-cli/utils/cli_io.h
        ../boxit-cli/services/package_management.cpp
        ../boxit-cli/services/package_management.h
        ../boxit-cli/services/service_base.cpp
        ../boxit-cli/services/service_base.h
        ../boxit-cli/global.h
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(${PROJECT_NAME}
        ${PROJECT_SOURCES}
    )
else()
    if(ANDROID)
        add_library(${PROJECT_NAME} SHARED
            ${PROJECT_SOURCES}
        )
    else()
        add_executable(${PROJECT_NAME}
          ${PROJECT_SOURCES}
        )
    endif()
endif()

target_compile_definitions(${PROJECT_NAME}
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
  PRIVATE
  libboxit
  OpenSSL::SSL
  OpenSSL::Crypto
  gRPC::grpc
  gRPC::grpc++
  Qt${QT_VERSION_MAJOR}::Core
  Qt${QT_VERSION_MAJOR}::Quick
  Qt${QT_VERSION_MAJOR}::QuickControls2
  Qt${QT_VERSION_MAJOR}::Widgets
  ${YAML_CPP_LIBRARIES}
  ${SECRET_LIBRARIES}
)

if (NO_SSL)
    target_compile_definitions(${PROJECT_NAME} PUBLIC NO_SSL)
endif()

target_include_directories(${PROJECT_NAME} PUBLIC ../boxit-cli ${SECRET_INCLUDE_DIRS})
target_compile_definitions(${PROJECT_NAME} PUBLIC QT_NO_KEYWORDS=1)
target_compile_definitions(${PROJECT_NAME} PRIVATE INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX})

install(TARGETS ${PROJECT_NAME})
