#pragma once
#include <QQuickItem>
#include <QString>
#include <protos/filesystem.grpc.pb.h>
#include <services/service_base.h>
#include <UploadQueue.h>

class FsService : public QObject, public service_base<FileSystem>
{
    Q_OBJECT
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(QString currentFileName READ currentFileName WRITE setCurrentFileName NOTIFY currentFileNameChanged)
    int m_progress = 100;

    QString m_currentFileName;

public:
    FsService(const std::shared_ptr<grpc::ChannelInterface>& channel,
              QObject* parent = nullptr):
        QObject(parent), service_base<FileSystem>(channel){};

    int progress() const
    {
        return m_progress;
    }

    QString currentFileName() const
    {
        return m_currentFileName;
    }

    std::atomic<bool> m_running;

public Q_SLOTS:
    QStringList getRoot();
    QStringList getBranch(const QString& branch);
    QStringList getRepo(const QString& branch, const QString& repo);
    QStringList parsePath(const QStringList& folders);
    bool removeFile(const QString& branch, const QString& repo, const QString& pkg);
    UploadQueue* startUpload();


    void sendFile(const QUrl& filename);

    bool registerPackage(const QString& branch, const QString& repo, const QString& pkg);


private:
    void setProgress(int progress);

    void setCurrentFileName(QString currentFileName)
    {
        if (m_currentFileName == currentFileName)
            return;

        m_currentFileName = currentFileName;
        Q_EMIT currentFileNameChanged(m_currentFileName);
    }

Q_SIGNALS:
    void progressChanged(int progress);
    void currentFileNameChanged(QString currentFileName);
};
