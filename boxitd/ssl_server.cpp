#include "ssl_server.h"
#include <thread>
#include <boost/asio.hpp>
#include <ostream>
#include <boost/log/trivial.hpp>
#include <utils.h>
#include <grpc++/grpc++.h>
#include <fstream>
#include <global.h>
#include <utils/boxit_auth_metadata_processor.h>

#include <controllers/package_controller.h>
#include <controllers/fs_controller.h>
#include <controllers/user_controller.h>
#include <controllers/log_controller.h>

inline std::string load_cert(const std::string& key_cert_file){

    auto path = global::settings.get<std::string>("certs_path").value_or("certs") + "/" + key_cert_file;
    std::ifstream cert_file(path);

    return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

}

void ssl_server::start()
{
    auto endpoint = "0.0.0.0:"+std::to_string(m_port);
    BOOST_LOG_TRIVIAL(info) << "\n" << utils::box;


    grpc::ServerBuilder builder;


    grpc::SslServerCredentialsOptions::PemKeyCertPair pkcp;
    pkcp.private_key = load_cert("server.key");
    pkcp.cert_chain = load_cert("server.crt");

    grpc::SslServerCredentialsOptions ssl_opts;
    ssl_opts.pem_root_certs="";
    ssl_opts.pem_key_cert_pairs.push_back(pkcp);

    auto creds = grpc::SslServerCredentials(ssl_opts);

    creds->SetAuthMetadataProcessor(std::shared_ptr<boxit_auth_metadata_processor>(new boxit_auth_metadata_processor));

    builder.AddListeningPort(endpoint, creds);

    builder.RegisterService(new package_controller);
    builder.RegisterService(new fs_controller);
    builder.RegisterService(new user_controller);
    builder.RegisterService(new log_controller);

    BOOST_LOG_TRIVIAL(info) << "Core :: Started BoxIt2 server at " << endpoint;

    auto server = builder.BuildAndStart();
    
    server->Wait();
}
