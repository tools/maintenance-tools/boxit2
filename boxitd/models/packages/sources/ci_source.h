#pragma once
#include "package_source.h"
#include <vector>
class ci_source: public package_source
{
public:
    ci_source(std::string repo):m_repo(repo){}

    // package_source interface
public:
    void download_to_pool(const std::string &package_name, const std::filesystem::path& pool) override;
    std::vector<package> get_avaiable_packages() override;

private:
    std::string m_repo;
};

class ci_source_factory: public package_source_factory {
public:
    virtual package_source* create_from_node(const YAML::Node &) override;
};
