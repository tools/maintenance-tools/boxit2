#pragma once
#include "package_source.h"
#include <string>
#include <vector>

#include <utils/https_downloader.h>
#include "package.h"

#include <boost/asio.hpp>
#include <boost/beast/ssl.hpp>

class repo_source: public package_source
{
public:
    repo_source(const std::string& name, const struct https_downloader::endpoint& ep);

    std::string name() const;
    void set_name(const std::string &value);

    struct endpoint get_endpoint() const;
    void set_endpoint(const endpoint &ep);

private:
    void download(const std::string &req_path, const std::filesystem::path& path);
    https_downloader m_downloader;
    std::string m_name;

    // package_source interface
public:
    void download_to_pool(const std::string &package_name,const std::filesystem::path& pool) override;
    std::vector<package> get_avaiable_packages() override;


};

class repo_source_factory: public package_source_factory {
public:
    virtual package_source* create_from_node(const YAML::Node &) override;
};
