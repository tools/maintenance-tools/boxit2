#include "ci_source.h"
#include <filesystem>
#include <global.h>
#include <boost/log/trivial.hpp>
#include <fmt/format.h>
void ci_source::download_to_pool(const std::string &package_name, const std::filesystem::__cxx11::path &pool)
{
    try{
        std::filesystem::copy(global::ci_cache_path / m_repo / package_name, pool);
    } catch(...){
        BOOST_LOG_TRIVIAL(error) << fmt::format("Unexpected error while pulling {} from ci cache folder, skipping...", package_name);
    }

    std::filesystem::remove(global::ci_cache_path / m_repo);
}

std::vector<package> ci_source::get_avaiable_packages()
{
   namespace fs = std::filesystem;

   auto iter = fs::directory_iterator(global::ci_cache_path / m_repo);

   std::vector<package> result;

   std::transform(iter, fs::directory_iterator{}, std::back_insert_iterator(result), [](const fs::directory_entry& entry){
       return package(entry.path().filename());
   });
   return result;
}

package_source* ci_source_factory::create_from_node(const YAML::Node &node)
{
    return new ci_source(node["name"].as<std::string>());
}
