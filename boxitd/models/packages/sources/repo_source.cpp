#include "repo_source.h"


#include <utils/pacman_db_parser.h>
#include <utils/archive_reader.h>

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <array>

#include "global.h"

#include <archive.h>
#include <archive_entry.h>

#include <Url.h>

#include <boost/log/trivial.hpp>

#include <fmt/format.h>

repo_source::repo_source(const std::string& name, const struct https_downloader::endpoint &ep):m_downloader(ep),m_name(name)
{}


std::string repo_source::name() const
{
    return m_name;
}

void repo_source::set_name(const std::string &value)
{
    m_name = value;
}


void repo_source::download(const std::string &req_path, const std::filesystem::path& path){
    boost::system::error_code ec;
    m_downloader.download(req_path, path, ec);

    if(ec){
        BOOST_LOG_TRIVIAL(error) <<  fmt::format("RepoSource :: Download of the package file {} failed after {} attempts",
                                                (m_downloader.endpoint().target + "/" + req_path), m_downloader.get_retry_count());
    }
}

void repo_source::download_to_pool(const std::string &package_name, const std::filesystem::path &pool)
{
    using namespace std::chrono_literals;

    BOOST_LOG_TRIVIAL(info) << fmt::format("RepoSource :: Getting package {} from repository {}", package_name, m_downloader.endpoint().string());
    download("/"+package_name,pool/package_name);

    std::this_thread::sleep_for(.4s);

    BOOST_LOG_TRIVIAL(info) << fmt::format("RepoSource :: Getting package signature {} from repository {}", (package_name + ".sig"), m_downloader.endpoint().string());

    download("/"+package_name+".sig", pool/(package_name+".sig"));
}

std::vector<package> repo_source::get_avaiable_packages()
{
    std::vector<package> result;

    boost::system::error_code ec;

    auto archive = m_downloader.download("/" + m_name+".db", ec);

    if(ec){
        BOOST_LOG_TRIVIAL(error) <<  fmt::format("RepoSource :: Download of the database file {} failed after {} attempts",
            (m_downloader.endpoint().target + "/" + m_name+ ".db"), m_downloader.get_retry_count());
        return result;
    }

    archive_reader reader(archive.data(), archive.size());

    for(const auto& entry : reader)
    {
        if(entry.path.ends_with("/desc")){

            std::vector<char> data = entry.data.read();
            data.push_back(0);


            pacman_db_parser parser{{reinterpret_cast<char*>(data.data())}};


            result.emplace_back(package{
                parser["FILENAME"]
            });
        }
    }

    return result;
}


package_source* repo_source_factory::create_from_node(const YAML::Node &node)
{
   struct https_downloader::endpoint ep;
    homer6::Url url(node["url"].as<std::string>());
    ep.host = url.getHost();
    ep.port = url.getPort()==0?443:url.getPort();
    ep.target = url.getPath();

    auto result = new repo_source(node["name"].as<std::string>(),
                                  ep);
    return result;

}
