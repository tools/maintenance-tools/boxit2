#include "package_repository.h"
#include <boost/range/iterator_range.hpp>
#include <boost/log/trivial.hpp>
#include <boost/process.hpp>
#include <thread>
#include <chrono>
#include <utils/pacman_db_parser.h>
#include "global.h"
#include <utils/archive_reader.h>
#include <mailio/smtp.hpp>
#include <fmt/format.h>

#include <archive.h>
#include <archive_entry.h>

#include "db_access/commands/add.h"
#include "db_access/commands/remove.h"
#include "db_access/commands/replace.h"
#include "db_access/commands/move.h"

#include "ci_transaction.h"

package_repository::package_repository(const std::filesystem::__cxx11::path &repository_name)
    :m_paths(global::settings.get<std::string>("root").value_or("repository"))
{
    if(!std::filesystem::exists(m_paths.repo())){
        std::filesystem::create_directories(m_paths.repo());
    }

    if(!std::filesystem::exists(m_paths.overlay())){
        std::filesystem::create_directories(m_paths.overlay());
    }

    if (!std::filesystem::exists(global::ci_cache_path)) {
        std::filesystem::create_directories(global::ci_cache_path);
    }

    load_from_yaml(repository_name);

    for(const auto& transaction
         : std::filesystem::directory_iterator(std::filesystem::absolute(global::ci_cache_path))){
        if(!transaction.path().filename().string().ends_with(".tar")){
            BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: '{}' is not a transaction archive!", transaction.path().filename().string());
            continue;
        }


        BOOST_LOG_TRIVIAL(info) << fmt::format("CI Transaction :: Found transaction archive '{}'", transaction.path().filename().string());

        process_ci_transaction(transaction.path());

    }


    global::watcher.add_watch(std::filesystem::absolute(global::ci_cache_path), [this](inotify_event* evt){
            if(!std::string(evt->name).ends_with(".tar")){
                BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: '{}' is not a transaction archive!", evt->name);
                return;
            }


            BOOST_LOG_TRIVIAL(info) << fmt::format("CI Transaction :: Found transaction archive '{}'", evt->name);

            process_ci_transaction(std::filesystem::absolute(global::ci_cache_path) / evt->name);

        }, IN_MOVED_TO | IN_CLOSE_WRITE);
}

bool package_repository::sync()
{
    if(m_is_in_sync){
        return false;
    }
    m_is_in_sync = true;
    std::thread thread([=,this](){
        for (auto& branch : m_syncable_branches){
            
            for( std::size_t i = 0; i < m_sources.size(); i++){
                
                auto it = m_sources.begin();
                std::advance(it, i);
                
                auto& repo = it->first;
                auto& sources = it->second;
                
                BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: Started synchronizing '{}' ({} sources)", repo, sources.size());
                
                for( auto& source : sources){
                    auto summary = download_pkgs(branch, repo, source.get());
                }
                

            }
        }
        m_is_in_sync = false;
    });
    thread.detach();
    return true;
}

bool package_repository::sync(const std::string &repo)
{
    if(m_is_in_sync){
        return false;
    }
    if(!m_repos.contains(repo)){
        throw std::invalid_argument("No such repository");
    }
    m_is_in_sync = true;
    std::thread thread([=, this](){
        for (auto& branch : m_syncable_branches){
            auto& sources = m_sources[repo];
            BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: Started synchronizing '{}' ({} sources)", repo, sources.size());

            for( auto& source : sources){
                download_pkgs(branch, repo, source.get());
            }
        }
        m_is_in_sync = false;
    });
    return true;
}

bool package_repository::snap(const branch_t &source_branch,
                              const branch_t &target_branch,
                              const repo_t &repo)
{

    if(!m_branches.contains(source_branch)){
        throw std::invalid_argument("No such source branch");
    }
    if(!m_branches.contains(target_branch)){
        throw std::invalid_argument("No such target branch");
    }
    
    using namespace db_access::commands;

    m_queue.enqueue(std::make_shared<class move>(options<std::initializer_list<class package>>{
                                                                .storage = package_storage::AUTO,
                                                                .branch = source_branch,
                                                                .repo = repo,
                                                                .package_list = {},
                                                              .callback = [=,this](status st){

                                                                  if(st==status::FAILED){
                                                                      return;
                                                                  }
                                                                  if(m_branches.contains(target_branch)){
                                                                      m_table[target_branch][repo].clear();

                                                                  }
                                                                  update_entries(target_branch, repo);

                                                              },
                                                                .paths = m_paths
                                                            }, target_branch));
    

    return true;
}

branch_comparison_t package_repository::compare(const branch_t &source_branch, const branch_t &target_branch)
{
    if(!m_branches.contains(source_branch)){
        throw std::invalid_argument("No such source branch");
    }
    if(!m_branches.contains(target_branch)){
        throw std::invalid_argument("No such target branch");
    }

    branch_comparison_t result;
    
    for(const auto& repo_name : m_repos) {
        auto source_repository = m_table[source_branch][repo_name];
        auto target_repository = m_table[target_branch][repo_name];
        
        
        for(const auto& package : source_repository){
            result[package.name()].first = package.version();
        }
        for(const auto& package : target_repository){
            result[package.name()].second = package.version();
        }
    }
    
    return result;
}


void package_repository::add_branch(const std::string &name, bool syncable)
{
    m_branches.emplace(name);
    for(auto& repo : m_repos) {
        update_entries(name, repo);
    }
    if(syncable){
        m_syncable_branches.emplace(name);
    }
}

void package_repository::add_repo(const std::string &name)
{
    m_repos.emplace(name);
    
    for(auto& br : m_branches) {
        update_entries(br,name);
    }
}

void package_repository::add_source(const repo_t &repo,package_source_ptr ptr)
{
    m_sources[repo].emplace_back(std::move(ptr));
}

std::set<package> package_repository::get_pkgs(const branch_t &branch, const repo_t &repo)
{
    if(m_branches.contains(branch) && m_repos.contains(repo)){
        
        return {m_table[branch][repo].begin(), m_table[branch][repo].end()};
    } else {
        throw std::invalid_argument("No such branch or repository");
    }
    
}

void package_repository::reload(const std::filesystem::__cxx11::path &repository_name)
{
    m_paths = repository_paths(global::settings.get<std::string>("root").value_or("./repository"));
    load_from_yaml(repository_name);
}

bool package_repository::remove(const branch_t &branch, const repo_t &repo, const package &package)
{
    using namespace db_access::commands;
    m_queue.enqueue(std::make_shared<class remove>(options<std::initializer_list<class package>>{
                                                                .storage = package_storage::AUTO,
                                                                .branch = branch,
                                                                .repo = repo,
                                                                .package_list = {package},
                                                                .callback = [=,this](status st){
                                                                    if(st==status::FAILED){
                                                                        return;
                                                                    }
                                                                    auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                                                                                              [=](auto& pkg){
                                                                        return pkg.name()==package.name();
                                                                    });
                                                                    m_table[branch][repo].erase(found);
                                                                },
                                                                .paths = m_paths
                                                            }));



    return true;
}


std::ofstream package_repository::overlay_file_stream(const std::string& filename)
{
    return std::ofstream{m_paths.overlay() / filename, std::ios::binary | std::ios::out | std::ios::app};
}


bool package_repository::register_package(const branch_t& branch, const repo_t& repo, const package& package)
{
    using namespace db_access::commands;
    
    auto relative_overlay = std::filesystem::relative(m_paths.overlay(), m_paths.terminal(branch,repo));

    if(!m_branches.contains(branch) || !m_repos.contains(repo)){
        BOOST_LOG_TRIVIAL(error) << fmt::format("PackageRepository :: Can't register package '{}': no such target branch or repository", package.filename());
        return false;
    }
    auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                              [=](auto& pkg){
        return pkg.name()==package.name();
    });
    
    if(found!=m_table[branch][repo].end()){

        if(found->version()==package.version()){
            BOOST_LOG_TRIVIAL(warning) << fmt::format("PackageRepository :: '{}' with the same version is already in the repository! Skipping...", package.name());
            return false;
        }
        
        BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: Changing '{}' version ('{}' over '{}')", package.name(), package.version().string(), found->version().string());


        m_queue.enqueue(std::make_shared<class remove>(options<std::initializer_list<class package>>{
                                                                    .storage = package_storage::AUTO,
                                                                    .branch = branch,
                                                                    .repo = repo,
                                                                    .package_list = {*found},
                                                                    .callback = [=,this](status st){
                                                                        if(st==status::FAILED){
                                                                            return;
                                                                        }
                                                                        m_table[branch][repo].erase(found);
                                                                    },
                                                                    .paths = m_paths
                                                                }));        
    }
    

    m_queue.enqueue(std::make_shared<class add>(options<std::initializer_list<class package>>{
                                                             .storage = package_storage::OVERLAY,
                                                             .branch = branch,
                                                             .repo = repo,
                                                             .package_list = {package},
                                                             .callback = [=,this](status st){
                                                                 if(st==status::FAILED){
                                                                     return;
                                                                 }
                                                                 m_table[branch][repo].emplace(package);
                                                             },
                                                             .paths = m_paths,
                                                         }));
    return true;
}


package_repository::summary package_repository::download_pkgs(const branch_t &branch, const repo_t &repo, package_source *source)
{
    using namespace std::chrono_literals;
    summary result;

    auto packages = source->get_avaiable_packages();
    BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: '{}': source has {} packages", repo, packages.size());

    boost::asio::thread_pool pool(std::thread::hardware_concurrency());

    for(auto& package : packages){
        

        if(m_table[branch][repo].contains(package))
        {
            BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: '{}' is already in repository, skipping...", package.filename());
            continue;
        }
        if(m_ignores[repo].contains(package.name()) ||
                m_global_ignores.contains(package.name()))
        {
            BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: '{}' is in ignore list, skipping...", package.name());
            continue;
        }

        std::this_thread::sleep_for(.4s);


        // Submit a lambda object to the pool.
        boost::asio::post(pool, [=, this, &result]() {
            source->download_to_pool(package.filename(), m_paths.repo());

            auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                                      [=](auto& pkg){
                                          return pkg.name()==package.name();
                                      });

            if(found != m_table[branch][repo].end()){
                result.removed.emplace_back(*found);
            }

            result.added.emplace_back(package);
        });

    }
    pool.join();
    {
    using namespace db_access::commands;
    
    m_queue.enqueue(std::make_shared<class remove>(options<std::vector<class package>>{
                                                                .storage = package_storage::AUTO,
                                                                .branch = branch,
                                                                .repo = repo,
                                                                .package_list = result.removed,
                                                                .callback = [=,this](status st){
                                                                    if(st==status::SUCCESS){

                                                                        for(const auto& pkg : result.removed){
                                                                            m_table[branch][repo].erase(pkg);
                                                                        }

                                                                    }
                                                                },
                                                                .paths = m_paths
                                                            }));
    
    m_queue.enqueue(std::make_shared<add>(options<std::vector<class package>>{
                                                             .storage = package_storage::REPO,
                                                             .branch = branch,
                                                             .repo = repo,
                                                             .package_list = result.added,
                                                             .callback = [=,this] (status st) {
                                                                 if(st==status::SUCCESS){
                                                                     BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: Synchronization of '{}' done", repo);


                                                                     for(const auto& pkg : result.added){
                                                                         m_table[branch][repo].emplace(pkg);
                                                                     }

                                                                 } else {
                                                                     BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: Synchronization of '{}' failed", repo);
                                                                 }
                                                             },
                                                             .paths = m_paths,

                                                         }));
    }
    return result;
}



void package_repository::update_entries(const branch_t& br,const repo_t& repo)
{
    
    m_table.emplace(br,repo, std::set<package>());
    if (std::filesystem::exists(m_paths.terminal(br,repo))){
        if(std::filesystem::exists(m_paths.database(br,repo).string()+".tar.xz")){
            archive_reader reader(m_paths.database(br,repo).string()+".tar.xz");


            for(const auto& entry : reader)
            {
                if(entry.path.ends_with("/desc")){

                    std::vector<char> data = entry.data.read();
                    data.push_back(0);


                    pacman_db_parser parser{{reinterpret_cast<char*>(data.data())}};

                    m_table[br][repo].emplace(package{
                        parser["FILENAME"]
                    });
                }
            }
        }
        BOOST_LOG_TRIVIAL(info) << fmt::format("PackageRepository :: {}/{} has {} packages", br, repo, m_table[br][repo].size());
    } else {
        std::filesystem::create_directories(m_paths.terminal(br,repo));
    }
}

void package_repository::load_from_yaml(const std::filesystem::__cxx11::path &filename)
{
    m_branches.clear();
    m_repos.clear();
    
    m_table.clear();
    
    try{
        auto node = YAML::LoadFile(filename);
        
        auto branches = global::settings.get<std::vector<std::string>>("branches");
        
        for(const auto &branch : branches.value_or(std::vector<std::string>())){
            if(branch=="unstable"){
                add_branch(branch,true);
            } else {
                add_branch(branch);
            }
        }
        auto glob_ignores = global::settings.get<YAML::Node>("ignores");
        
        for(const auto& ignores : glob_ignores.value_or(YAML::Node())) {
            import_ignores(ignores);
        }
        
        for (const auto &repo : node){
            
            add_repo(repo.first.as<std::string>());
            
            for (const auto& source : repo.second["sources"]){
                if(source["type"].as<std::string>()=="repository"){
                    add_source(repo.first.as<std::string>(),
                               std::unique_ptr<package_source>(repo_source_factory().create_from_node(source)));
                }
            }
            for (const auto& ignores : repo.second["ignores"]){
                import_ignores(ignores, repo.first.as<std::string>());
            }
        }
    } catch (YAML::BadFile& exception){
        
    }
    if(m_repos.size()==0 || m_branches.size()==0){
        BOOST_LOG_TRIVIAL(warning) << "PackageRepository :: No branches or repositories are configured, BoxIt2 server will be malfunctioned. Check your settings.";
    }
}

std::set< std::string > package_repository::get_signatures(const branch_t& branch, const repo_t& repo)
{
    std::set<std::string> result;
    for(const auto& package : m_table[branch][repo]){
        if(std::filesystem::exists(m_paths.signature(branch,repo,package.filename()))){
            result.insert(package.filename()+".sig");
        }
    }
    return result;
}

void package_repository::import_ignores(const YAML::Node& nd, const std::string& repo)
{
    
    auto& ignorelist = repo.empty()?m_global_ignores:m_ignores[repo];
    try{
        if(nd.IsMap()){
            if(nd["type"].as<std::string>()!="file"){
                return;
            }
            std::ifstream str(nd["path"].as<std::string>());
            
            std::string ignore;
            int num_of_ignores = 0;
            
            while(std::getline(str,ignore)){
                
                boost::trim(ignore);
                
                if(ignore.empty() || ignore.starts_with("#")){
                    continue;
                }
                
                ignorelist.insert(ignore);
                num_of_ignores++;
            }
            BOOST_LOG_TRIVIAL(info)<<fmt::format("PackageRepository :: Imported ignore list '{}' with {} ignored packages for '{}'", nd["path"].as<std::string>(), num_of_ignores, repo.empty()?"global":repo);
        }
        else if (nd.IsSequence()){
            for(const auto& ignore : nd){
                ignorelist.insert(ignore.as<std::string>());
            }
        }
    } catch (std::exception& e){}
    
}

void package_repository::process_ci_transaction(const std::filesystem::path& path)
{
    using namespace std::string_literals;
    ci_transaction transaction;
    try{

        archive_reader rdr(path);

        bool pkg_found = false;
        bool signature_found = false;
        bool details_found = false;

        for(const auto& entry : rdr){

            if(entry.path.ends_with(".yml")){
                std::vector<char> contents;
                try{
                    if(details_found){
                        continue;
                    }
                    contents = entry.data.read();
                    contents.emplace_back('\0');
                    transaction = ci_transaction::from_yaml(YAML::Load(contents.data()));
                    transaction.pkgname = path.filename();

                    transaction.pkgname = transaction.pkgname.substr(0, transaction.pkgname.size()-".tar"s.length());

                    BOOST_LOG_TRIVIAL(info) << fmt::format("CI Transaction :: Found correct transaction details for '{}'", transaction.pkgname);
                    details_found = true;

                } catch(const YAML::Exception& e){
                    BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: Transaction file is invalid!");
                    BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: Transaction file contents\n{}", contents.data());
                    BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: Yaml-cpp error:\n{}", e.what());
                    details_found = false;
                }

            } else {
                if (entry.path.ends_with(".pkg.tar.zst")){
                    pkg_found = true;
                } else if (entry.path.ends_with(".pkg.tar.zst.sig")){
                    signature_found = true;
                } else {
                    continue;
                }
                entry.data.read_into_file(m_paths.overlay() / entry.path);
                BOOST_LOG_TRIVIAL(info) << fmt::format("CI Transaction :: Unpacked '{}' file into overlay folder", entry.path);
            }

        }

        if(pkg_found && signature_found && details_found){
            register_package(transaction.branch, transaction.repository, transaction.pkgname+".pkg.tar.zst");
        } else {
            BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: Archive '{}' is malformed! Cleaning up...", path.filename().string());

            std::string pkgname = path.filename();
            pkgname = pkgname.substr(0, pkgname.size() -".tar"s.length());

            std::filesystem::remove(m_paths.overlay() / (pkgname + ".pkg.tar.zst"));
            std::filesystem::remove(m_paths.overlay() / (pkgname + ".pkg.tar.zst.sig"));
        }

    } catch (const archive_exception& e){
        BOOST_LOG_TRIVIAL(error) << fmt::format("CI Transaction :: Can't open an archive '{}'. LibArchive error string:\n{}", path.filename().string(), e.what());
    }
    std::filesystem::remove(std::filesystem::absolute(global::ci_cache_path) / path.filename());

}
