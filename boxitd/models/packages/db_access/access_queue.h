#pragma once
#include <queue>
#include <list>
#include <variant>
#include <memory>
#include <mutex>
#include <thread>
#include <types.h>
#include <package.h>
#include <utils/repository_paths.h>
#include "commands/base.h"

namespace db_access {

class queue
{
public:
    queue() = default;

    bool enqueue(const std::shared_ptr<commands::base>& request);


private:
    void process();

    std::atomic<bool> m_is_processing;
    std::mutex m_queue_mutex;
    std::queue<std::shared_ptr<commands::base>, std::list<std::shared_ptr<commands::base>>> m_queue;

};

}
