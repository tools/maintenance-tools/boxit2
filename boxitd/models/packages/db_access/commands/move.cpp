#include "move.h"
#include <filesystem>
#include <boost/log/trivial.hpp>
#include <fmt/format.h>
namespace db_access::commands {

void move::operator ()()
{
    BOOST_LOG_TRIVIAL(info) << fmt::format("Moving '{0}/{2}' to '{1}/{2}'", m_branch, m_target_branch, m_repo);

    namespace fs = std::filesystem;
    
    bool success = true;
    
    for(const auto& file
        : fs::directory_iterator(m_paths->terminal(m_target_branch, m_repo))){
        success &= fs::remove(file);
    }
    
    std::error_code ecode;

    fs::copy(m_paths->terminal(m_branch, m_repo),
             m_paths->terminal(m_target_branch, m_repo),
             fs::copy_options::recursive|fs::copy_options::copy_symlinks,
             ecode);
    
    success &= !ecode.value();
    
    m_callback(success?status::SUCCESS:status::FAILED);

}

}
