#pragma once
#include "base.h"

namespace db_access::commands {

class add : public base {
public:
    template<typename TContainer>
    add(const options<TContainer>&opts):base(opts){}

    virtual void operator()() override {
        for(const auto& pkg : m_package_list){
            create_symlink(pkg);
        }
        auto status = repo_add()
                          ?status::SUCCESS
                          :status::FAILED;
        m_callback(status);
        
    }

private:
    bool repo_add();
    bool create_symlink(const package& pkg);
};

}
