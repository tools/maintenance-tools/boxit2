#pragma once
#include "base.h"

namespace db_access::commands {


class replace : public base {
public:
    template<typename TContainer>
    replace(const options<TContainer>&opts):base(opts){}
};

}
