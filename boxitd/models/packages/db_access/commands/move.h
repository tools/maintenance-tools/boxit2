#pragma once
#include "base.h"

namespace db_access::commands {

class move : public base
{
public:
    template<typename TContainer>
    move(const options<TContainer>&opts, const branch_t& target): base(opts), m_target_branch(target){}

    // base interface
public:
    virtual void operator ()() override;

private:
    branch_t m_target_branch;
};

}
