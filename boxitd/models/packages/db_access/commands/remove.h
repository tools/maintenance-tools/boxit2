#pragma once
#include "base.h"
namespace db_access::commands {

class remove : public base {
public:
    template<typename TContainer>
    remove(const options<TContainer>&opts):base(opts){}

    void operator()() override {
        for(const auto& pkg : m_package_list){
            remove_symlink(pkg);
        }
        auto status = repo_remove()
                          ?status::SUCCESS
                          :status::FAILED;
        m_callback(status);
    };
private:
    bool repo_remove();
    bool remove_symlink(const package& pkg);

};
}
