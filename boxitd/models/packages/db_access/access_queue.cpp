#include "access_queue.h"

#include <filesystem>
#include <boost/process.hpp>
#include <boost/log/trivial.hpp>
#include <fmt/format.h>

namespace db_access {

bool queue::enqueue(const std::shared_ptr<commands::base>& request) {
    {
        std::scoped_lock guard(m_queue_mutex);
        m_queue.push(request);
    }
    if(!m_is_processing){
        process();
    }
    
    return true;
}

void queue::process() {
    
    std::thread processor ([this](){
        m_is_processing = true;
        bool is_empty = true;
        {
            std::scoped_lock guard(m_queue_mutex);
            is_empty = m_queue.empty();
        }
        while(!is_empty){
            std::scoped_lock guard(m_queue_mutex);
            auto request = m_queue.front();
            m_queue.pop();
            (*request)();
            is_empty = m_queue.empty();
        }
        m_is_processing = false;
    });
    processor.detach();
    
}

}
