#pragma once

#include <chrono>
#include <string>
#include <filesystem>
#include <yaml-cpp/yaml.h>

struct ci_transaction {
    std::string pkgname;

    std::chrono::time_point<std::chrono::system_clock> transaction_time;
    std::string sha256_sum;
    std::string repository;
    std::string branch;


    static ci_transaction from_yaml(const YAML::Node& transaction_yaml){

        if(!transaction_yaml.IsMap()){
            throw new std::ios_base::failure("Transaction file is not valid");
        }
        ci_transaction result;

//        result.sha256_sum = transaction_yaml["sha256_pkg"].as<std::string>();
        result.repository = transaction_yaml["repository"].as<std::string>();
        result.branch = transaction_yaml["branch"].as<std::string>();

        return result;
    }
};
