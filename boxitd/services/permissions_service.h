#pragma once
#include <models/permission.h>
namespace permissions_service
{
    bool check(const std::string& name, const permission& perm);

    void add_permission(const std::string& name, const permission& perm);

    void remove_permission(const std::string& name, const permission& perm);
};
