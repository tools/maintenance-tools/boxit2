#include "auth_service.h"
#include <iostream>
#include <utils.h>
auth_service::auth_service()
{

}

bool auth_service::authorize(const std::string &name, const std::string &pwd)
{

    SQLite::Statement query(global::database, "SELECT * FROM users WHERE name = ? AND password = ?");

    query.bind(1, name);
    query.bind(2, utils::sha256(pwd));


    return query.executeStep();
}
