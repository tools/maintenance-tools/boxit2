#pragma once
#include <map>
#include <string>
#include <set>
#include "package.h"
#include "double_map.h"
#include <models/packages/package_repository.h>
class package_service
{
public:
    static bool sync(const std::string& = "");
    static bool snap(const branch_t& from, const branch_t& to);
    static branch_comparison_t compare(const branch_t& source, const branch_t& target);


    static std::set<branch_t> branches();
};
