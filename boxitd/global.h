#pragma once
#include <SQLiteCpp/Database.h>
#include <string>
#include <map>
#include <models/packages/package_repository.h>
#include <models/packages/sources/repo_source.h>
#include <settings_manager.h>
#include <utils/fs_watcher.h>
#include <filesystem>
#include <boost/log/trivial.hpp>

class initializer {
public:
    class initialization_error : public std::exception {
    public:
        explicit initialization_error(const std::string& what):message(what){}
    private:
        std::string message;
        // exception interface
    public:
        virtual const char *what() const noexcept override;
    };
private:
    void find_scripts();
    void setup_db(SQLite::Database& db);
    void setup_watcher(fs_watcher& watcher);
    void setup_logger();
    
public:
    initializer();
};

namespace global {
    
    inline SQLite::Database database("database.sqlite",SQLite::OPEN_READWRITE|SQLite::OPEN_CREATE);
    
    inline fs_watcher watcher;

    inline initializer init;

    inline settings_manager settings("config.yml");

    inline const std::filesystem::path sync_path = "pool/sync/";

    inline const std::filesystem::path overlay_path = "pool/overlay/";

    inline const std::filesystem::path ci_cache_path = settings.get<std::string>("ci-cache-path").value_or("ci-cache");

    inline package_repository repository("repositories.yml");

}
