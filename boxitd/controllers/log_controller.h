#pragma once
#include <protos/log.grpc.pb.h>
class log_controller : public LogController::Service
{
public:

    virtual grpc::Status GetLog(grpc::ServerContext *context, const GetLogRequest *request, GetLogResponse *response) override;
    virtual grpc::Status GetErrors(grpc::ServerContext *context, const GetErrorsRequest *request, GetErrorsResponse *response) override;
};
