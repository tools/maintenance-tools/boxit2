#include "package_controller.h"
#include <services/package_service.h>
#include <algorithm>
#include <services/permissions_service.h>
#include "global.h"
#include <boost/log/trivial.hpp>
#include <fmt/format.h>
#include <utils.h>
::grpc::Status package_controller::Snap(::grpc::ServerContext* context, const ::SnapshotRequest* request, ::SnapshotReply* response)
{
    try{
        branch_t source = request->source_branch();
        branch_t target = request->target_branch();



        if(!permissions_service::check(utils::extract_metadata(context, "username") , "packages.snap."+source+"."+target)){
            return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, fmt::format("User cannot snapshot {} to {}", source, target));
        }

        bool result = package_service::snap(source, target);
        response->set_ok(result);
    } catch (const std::invalid_argument& exception) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, exception.what());
    } catch(const std::exception& exception){
        return grpc::Status(grpc::StatusCode::INTERNAL, exception.what());
    }

    return grpc::Status::OK;
}

::grpc::Status package_controller::Sync(::grpc::ServerContext* context, const ::SyncRequest* request, ::SyncReply* response)
{
    try{
        if(!permissions_service::check(utils::extract_metadata(context, "username"), "packages.sync."+request->repo())){
            return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, fmt::format("User cannot sync {} ", request->repo()));
        }

        bool result = package_service::sync(request->repo());
        response->set_ok(result);
    } catch (const std::invalid_argument& exception) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, exception.what());
    } catch(const std::exception& exception){
        return grpc::Status(grpc::StatusCode::INTERNAL, exception.what());
    }
    
    return grpc::Status::OK;

}

grpc::Status package_controller::Compare(grpc::ServerContext *context, const CompareRequest *request, CompareReply *response)
{
    try {
        if(!permissions_service::check(utils::extract_metadata(context, "username"), "packages.compare."+request->source_branch()+"."+request->target_branch())){
            return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, fmt::format("User cannot compare {} with {}", request->source_branch(), request->target_branch()));
        }

        auto branch_comparison = package_service::compare(request->source_branch(), request->target_branch());


        for(const auto& el : branch_comparison) {

            EVRPair pair;

            EVR &first = *pair.mutable_first(),
                    &second = *pair.mutable_second();
            first.set_epoch(el.second.first.epoch);
            first.set_revision(el.second.first.release);
            first.set_version(el.second.first.version);

            second.set_epoch(el.second.second.epoch);
            second.set_revision(el.second.second.release);
            second.set_version(el.second.second.version);



            (*response->mutable_comparison())[el.first] = pair;
        }
    }
    catch (const std::invalid_argument& exception) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, exception.what());
    } catch(const std::exception& exception){
        return grpc::Status(grpc::StatusCode::INTERNAL, exception.what());
    }

    return grpc::Status::OK;
}
