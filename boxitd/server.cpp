#include "server.h"
#include <thread>
#include <boost/asio.hpp>
#include <ostream>
#include <boost/log/trivial.hpp>
#include <utils.h>
#include <grpc++/grpc++.h>
#include <fstream>
#include <global.h>
#include <utils/boxit_auth_metadata_processor.h>

#include <controllers/package_controller.h>
#include <controllers/fs_controller.h>
#include <controllers/user_controller.h>
#include <controllers/log_controller.h>

void server::start()
{
    auto endpoint = "0.0.0.0:"+std::to_string(m_port);
    BOOST_LOG_TRIVIAL(info) << "\n" << utils::box;

    grpc::ServerBuilder builder;

    auto creds = grpc::InsecureServerCredentials();

    builder.AddListeningPort(endpoint, creds);

    builder.RegisterService(new package_controller);
    builder.RegisterService(new fs_controller);
    builder.RegisterService(new user_controller);
    builder.RegisterService(new log_controller);

    BOOST_LOG_TRIVIAL(info) << "Core :: Started BoxIt2 (insecure) server at " << endpoint;

    auto server = builder.BuildAndStart();
    
    server->Wait();
}
