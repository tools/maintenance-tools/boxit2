#pragma once
#include <streambuf>
#include <grpc++/grpc++.h>
#include <protos/filesystem.grpc.pb.h>
#include <boost/log/trivial.hpp>
#include <utils.h>
class grpc_streambuf : public std::streambuf {

public:
    grpc_streambuf(grpc::ServerReader<WriteRequest>* reader) : m_message_reader (reader)
    {}

    // basic_streambuf interface
protected:

    bool reload() {
            WriteRequest rq;
            if(!m_message_reader->Read(&rq)){
                BOOST_LOG_TRIVIAL(info) << "end of reading";
                return false;
            }

            if(rq.content_case()!=WriteRequest::ContentCase::kDataBlock){
                throw std::invalid_argument("You should read metadata first");
            }

            m_data_buffer = rq.data_block().data();
            size += rq.data_block().data().size();
            BOOST_LOG_TRIVIAL(info) << "0x" << std::hex << size;


            setg(m_data_buffer.data(),&m_data_buffer[0], &m_data_buffer[0]+rq.data_block().size());

            return true;

    }



    std::streamsize xsgetn(char* s, std::streamsize n) override
    {
        for (std::streamsize i = 0; i != n; ++i) {
            if (gptr() == egptr()) {
                return i;
            }
            *s++ = *_M_in_cur++;
        }
        return n;
    }
    virtual int_type underflow() override
    {
        return (gptr() == egptr() && !reload()) ? traits_type::eof() : *_M_in_cur;
    }

    int_type uflow() override
    {
        return (gptr() == egptr() && !reload()) ? traits_type::eof() : *_M_in_cur++;
    }

private:
    uint64_t size;
    std::string m_data_buffer;
    grpc::ServerReader<WriteRequest>* m_message_reader;

};
