#include "archive_reader.h"
#include <fcntl.h>

archive_reader::archive_reader(const std::filesystem::path &file)
{
    archive_read_support_filter_all(m_archive.get());
    archive_read_support_format_all(m_archive.get());

    auto status = archive_read_open_filename(m_archive.get(), file.c_str(), 1024);

    if(status != ARCHIVE_OK){
        throw archive_exception(archive_error_string(m_archive.get()));
    }

}

archive_reader::archive_reader(void *data, int size)
{
    archive_read_support_filter_all(m_archive.get());
    archive_read_support_format_all(m_archive.get());

    auto status =archive_read_open_memory(m_archive.get(), data, size);

    if(status != ARCHIVE_OK){
        throw archive_exception(archive_error_string(m_archive.get()));
    }
}

std::vector<char> archive_data::read(int block_size) const {
    size_t sz = 0;
    std::vector<char> result;
    std::vector<char> buffer(block_size);

    do{
        sz = archive_read_data(m_archive, buffer.data(), buffer.size());
        result.resize(result.size()+sz);

        std::copy_n(buffer.begin(), sz, result.end()-sz);

    } while (sz==buffer.size());

    return result;
}

bool archive_data::read_into_file(const std::filesystem::__cxx11::path &path) const
{
    auto fd = open(path.c_str(), O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU|S_IRGRP|S_IROTH);

    if(fd==-1){
        return false;
    }

    auto status = archive_read_data_into_fd(m_archive, fd);

    if(status!=ARCHIVE_OK){return false;}

    status = close(fd);

    return status!=-1;
}
