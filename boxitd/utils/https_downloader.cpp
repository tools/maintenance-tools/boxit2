#include "https_downloader.h"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/algorithm/string.hpp>

#include <fmt/format.h>
#include <boost/log/trivial.hpp>

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
namespace ssl = net::ssl;
using tcp = net::ip::tcp;

template<class TResponse>
void download(
    boost::beast::http::request<http::string_body> req,
    boost::beast::ssl_stream<boost::beast::tcp_stream> &stream,
    TResponse &res,
    boost::system::error_code& ec,
    int retry_count = 5)
{
    beast::flat_buffer buffer;

    auto req_path = req.target().to_string();

    do {

        BOOST_LOG_TRIVIAL(info) <<fmt::format("HTTPS :: Downloading {}...", req_path);

        http::write(stream, req, ec);

        if(ec && retry_count>0){
            BOOST_LOG_TRIVIAL(warning) << fmt::format("HTTPS :: Download of {} has failed! Retrying...", req_path);
            retry_count--;
            continue;
        }

        http::read(stream, buffer, res, ec);

        if(ec && retry_count>0){
            BOOST_LOG_TRIVIAL(warning) << fmt::format("HTTPS :: Download of {} has failed! Retrying...", req_path);
            retry_count--;
            continue;
        }

    } while(ec && retry_count>0);


}

std::string https_downloader::endpoint::string() const {
    return fmt::format("{}:{}/{}", host, port, target);
}

https_downloader::https_downloader(struct endpoint ep, const std::string &user_agent):
    m_ep(ep),
    m_user_agent(user_agent),
    m_ctx(ssl::context::tlsv12_client)
{
    m_ctx.set_default_verify_paths();
    m_ctx.set_verify_mode(ssl::verify_peer);
}



void https_downloader::download(const std::string& req_path,
                                const std::filesystem::__cxx11::path &path,
                                boost::system::error_code& ec)
{
    net::io_context ioc;
    auto stream = setup_stream(ioc);

    http::response_parser<http::file_body> res;

    res.body_limit(std::numeric_limits<uint64_t>::max());
    res.get().body().open(path.c_str(),beast::file_mode::write,ec);
    if(ec){
        BOOST_LOG_TRIVIAL(error) << fmt::format("HTTPS :: Failed to open {}! Skipping download...", path.string());
        return;
    }

    ::download(setup_request(req_path), stream, res, ec);


    if(ec){
        BOOST_LOG_TRIVIAL(error) <<fmt::format("HTTPS :: Download of {} as {} has failed after {} attempts", req_path, m_retry_count, path.string());
    } else {
        BOOST_LOG_TRIVIAL(info) <<fmt::format("HTTPS :: Downloaded {} as {}",req_path, path.string());
    }

    stream.shutdown(ec);

    res.get().body().close();
}

std::vector<uint8_t> https_downloader::download(const std::string& req_path,
                                                boost::system::error_code& ec)
{
    net::io_context ioc;
    auto stream = setup_stream(ioc);

    beast::flat_buffer buffer;

    http::response<http::vector_body<uint8_t>> res;

    ::download(setup_request(req_path), stream, res, ec, m_retry_count);

    stream.shutdown(ec);

    return res.body();
}


boost::beast::ssl_stream<beast::tcp_stream> https_downloader::setup_stream(boost::asio::io_context &ioc)
{
    tcp::resolver resolver(ioc);
    beast::ssl_stream<beast::tcp_stream> stream(ioc, m_ctx);

    SSL_set_tlsext_host_name(stream.native_handle(), m_ep.host.c_str());

    auto const results = resolver.resolve(m_ep.host, std::to_string(m_ep.port));

    beast::get_lowest_layer(stream).connect(results);
    stream.handshake(ssl::stream_base::client);

    return stream;
}

boost::beast::http::request<http::string_body> https_downloader::setup_request(const std::string &req_path) const
{
    http::request<http::string_body> req
        {
            http::verb::get,
            m_ep.target + '/' + req_path,
            10
        };

    req.set(http::field::host, m_ep.host);
    req.set(http::field::user_agent, m_user_agent);

    return req;

}

struct https_downloader::endpoint https_downloader::endpoint() const
{
    return m_ep;
}

void https_downloader::set_endpoint(const struct endpoint &new_ep)
{
    m_ep = new_ep;
}

int https_downloader::get_retry_count() const
{
    return m_retry_count;
}

void https_downloader::set_retry_count(int new_retry_count)
{
    m_retry_count = new_retry_count;
}

