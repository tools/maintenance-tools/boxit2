#include "boxit_auth_metadata_processor.h"
#include <boost/log/trivial.hpp>
#include <services/auth_service.h>

grpc::Status boxit_auth_metadata_processor::Process(const InputMetadata &auth_metadata,
                                                    grpc::AuthContext *context,
                                                    OutputMetadata *consumed_auth_metadata,
                                                    [[maybe_unused]] OutputMetadata *response_metadata)
{

    auto username_it = auth_metadata.find("username");
    auto password_it = auth_metadata.find("password");
    if (username_it == auth_metadata.end() || password_it == auth_metadata.end()){
        BOOST_LOG_TRIVIAL(info) <<"Missing Credentials";
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "Missing Credentials");
    }

    std::string username {username_it->second.begin(), username_it->second.end()};
    std::string password {password_it->second.begin(), password_it->second.end()};

    if (!auth_service::authorize(username, password)){
        BOOST_LOG_TRIVIAL(info) << "Credentials do not match";
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "Credentials do not match");
    }

    consumed_auth_metadata->insert({"username", username});
    consumed_auth_metadata->insert({"password", password});

    context->AddProperty("username", username);
    context->SetPeerIdentityPropertyName("username");

    return grpc::Status::OK;
}
