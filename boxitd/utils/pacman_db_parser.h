#pragma once
#include <string>

class pacman_db_parser
{
public:
    pacman_db_parser(const std::string& inmemory_desc);

    std::string operator[](const std::string& key) const;
    std::string get(const std::string& key) const;
private:

    std::string m_contents;
};
