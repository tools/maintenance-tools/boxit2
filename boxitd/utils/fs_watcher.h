#pragma once
#include <sys/inotify.h>

#include <string>
#include <functional>
#include <thread>
#include <filesystem>
#include <atomic>

class fs_watcher
{
public:
    fs_watcher();
    ~fs_watcher();
    void add_watch(const std::filesystem::path& path, std::function<void(inotify_event *)>, uint32_t mask = IN_MODIFY | IN_DELETE);
private:
    std::thread m_main_loop_thread;
    std::atomic<int> m_inotify_handle_fd;
    std::unordered_map<int, std::pair<std::string, std::function<void(inotify_event *)>>> m_watch_handlers;
};
