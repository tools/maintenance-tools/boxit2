#include "boxit_message_builder.h"
#include "fmt/format.h"
#include "boost/range/adaptor/indexed.hpp"

namespace boxit2 {
namespace notification {


boxit_message_builder::boxit_message_builder(std::string title)
{
    m_message.m_title = title;
}

boxit_message_builder::operator boxit_message() const
{
    return std::move(m_message);
}

boxit_message_builder &boxit_message_builder::append(const std::string text)
{
    m_message.m_content.append(fmt::format("{}\r\n", text));

    return *this;
}

boxit_message_builder &boxit_message_builder::append_with_tag(const std::string tag,
                                                          const std::string text)
{
    return append(fmt::format("{}: {}", tag, text));
}

boxit_message_builder &boxit_message_builder::append_list(const std::initializer_list<std::string> list)
{
    return append(create_list(list));
}

boxit_message_builder &boxit_message_builder::append_tagged_list(
    const std::string tag, const std::initializer_list<std::string> list)
{
    return append_with_tag(tag, create_list(list));
}

std::string boxit_message_builder::create_list(const std::initializer_list<std::string> &list) const
{
    std::string temp = "";

    for (auto elem : list | boost::adaptors::indexed(1)) {
        temp.append(fmt::format("{}. {}\n", elem.index(), elem.value()));
    }

    return temp;
}

} // namespace notification
} // namespace boxit2
