#pragma once

#include "boxit_message.h"
#include <string>
#include "initializer_list"

namespace boxit2 {
namespace notification {

class boxit_message_builder
{
public:
    explicit boxit_message_builder(std::string title);

    operator boxit_message() const;

    boxit_message_builder &append(const std::string text);
    boxit_message_builder &append_with_tag(const std::string tag, const std::string text);
    boxit_message_builder &append_list(const std::initializer_list<std::string> list);
    boxit_message_builder &append_tagged_list(const std::string tag,
                                            const std::initializer_list<std::string> list);

private:
    std::string create_list(const std::initializer_list<std::string> &list) const;
private:
    boxit_message m_message;
};

} // namespace notification
} // namespace boxit2

