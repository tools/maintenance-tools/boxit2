#pragma once

#include "boxit_message.h"
#include <vector>
#include <string>

namespace boxit2 {
namespace notification {

class base_boxit_message_provider
{
public:
    enum SendResult { Success, Failure };

    virtual ~base_boxit_message_provider() = default;

    virtual SendResult send(const boxit_message &message) = 0;

    void append_recipient(const std::string &recipient);

private:
    virtual bool provider_is_valid() const = 0;
protected:
    std::vector<std::string> m_recipients;
};

} // namespace notification
} // namespace boxit2

