#include "boxit_message.h"
#include "fmt/format.h"
#include "boxit_message_builder.h"

namespace boxit2 {
namespace notification {


const std::string &boxit_message::content() const
{
    return m_content;
}

std::string boxit_message::to_string() const
{
    return fmt::format("{}\r\n{}", m_title, m_content);
}

boxit_message_builder boxit_message::make(std::string title)
{
    return boxit_message_builder(title);
}

const std::string &boxit_message::title() const
{
    return m_title;
}

} // namespace notification
} // namespace boxit2
