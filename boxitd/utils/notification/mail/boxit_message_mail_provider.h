#pragma once

#include "utils/notification/base_boxit_message_provider.h"
#include <mailio/smtp.hpp>
#include <mailio/pop3.hpp>
#include <mailio/imap.hpp>
#include <string>

namespace boxit2::notification::mail {

class boxit_message_mail_provider : public base_boxit_message_provider
{
public:
    boxit_message_mail_provider() = default;

    SendResult send(const boxit_message &message);
    SendResult send(const boxit_message &message, const mailio::smtps::auth_method_t &auth_method);

    void set_hostname(const std::string &newHostname);
    void set_port(int newPort);
    void set_username(const std::string &newUsername);
    void set_password(const std::string &newPassword);

private:
    SendResult submit_smtp(
        const mailio::message &message,
        const mailio::smtps::auth_method_t &auth_method = mailio::smtps::auth_method_t::START_TLS);

    mailio::message convert_boxit_message(const boxit_message &message) const;
    bool provider_is_valid() const;

private:
    // connection varibles
    std::string m_hostname;
    int m_port;

    // auth variables
    std::string m_username;
    std::string m_password;
};

} // namespace boxit2::notification::mail

