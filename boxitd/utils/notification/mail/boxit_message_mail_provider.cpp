#include "boxit_message_mail_provider.h"
#include <mailio/mime.hpp>
#include <fmt/format.h>

namespace boxit2::notification::mail {

base_boxit_message_provider::SendResult boxit_message_mail_provider::send(const boxit_message &message)
{
    return submit_smtp(convert_boxit_message(message));
}

base_boxit_message_provider::SendResult boxit_message_mail_provider::send(
    const boxit_message &message, const mailio::smtps::auth_method_t &auth_method)
{
    return submit_smtp(convert_boxit_message(message), auth_method);
}

base_boxit_message_provider::SendResult boxit_message_mail_provider::submit_smtp(
    const mailio::message &message, const mailio::smtps::auth_method_t &auth_method)
{
    if (!provider_is_valid())
        throw std::invalid_argument(fmt::format("Provider: [BoxitMessageMailProvider] not a valid!"));

    try {
        mailio::smtps client(m_hostname, m_port);

        client.authenticate(m_username, m_password, auth_method);
        client.submit(message);

        return SendResult::Success;
    } catch (...) {
        return SendResult::Failure;
    }
}

mailio::message boxit_message_mail_provider::convert_boxit_message(const boxit_message &message) const
{
    mailio::message tmp_msg;

    tmp_msg.subject(message.title());
    tmp_msg.content(message.content());
    tmp_msg.from({"boxitd", m_username});

    for (const auto &recipient : m_recipients) {
        tmp_msg.add_recipient({"", recipient});
    }

    tmp_msg.content_transfer_encoding(mailio::mime::content_transfer_encoding_t::QUOTED_PRINTABLE);

    return tmp_msg;
}

bool boxit_message_mail_provider::provider_is_valid() const
{
    return !m_username.empty() && !m_hostname.empty() && !m_password.empty() ? true : false;
}

void boxit_message_mail_provider::set_hostname(const std::string &newHostname)
{
    m_hostname = newHostname;
}

void boxit_message_mail_provider::set_port(int newPort)
{
    m_port = newPort;
}

void boxit_message_mail_provider::set_username(const std::string &newUsername)
{
    m_username = newUsername;
}

void boxit_message_mail_provider::set_password(const std::string &newPassword)
{
    m_password = newPassword;
}


} // namespace boxit2::notification::mail
