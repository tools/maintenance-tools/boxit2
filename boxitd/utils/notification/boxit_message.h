#pragma once

#include <string>

namespace boxit2 {
namespace notification {

class boxit_message_builder;

class boxit_message
{
public:
    friend class boxit_message_builder;

    static boxit_message_builder make(std::string title);

    const std::string &title() const;
    const std::string &content() const;
    std::string to_string() const;

private:
    boxit_message() = default;

    std::string m_title;
    std::string m_content;
};

} // namespace notification
} // namespace boxit2
