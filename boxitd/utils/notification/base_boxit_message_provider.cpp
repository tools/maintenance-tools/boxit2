#include "base_boxit_message_provider.h"
#include <stdexcept>

namespace boxit2 {
namespace notification {

void base_boxit_message_provider::append_recipient(const std::string &recipient)
{
    if (recipient.empty())
        throw std::invalid_argument("value cannot be empty.");

    m_recipients.emplace_back(recipient);
}

} // namespace notification
} // namespace boxit2
