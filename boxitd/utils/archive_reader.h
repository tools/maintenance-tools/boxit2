#pragma once

#include <memory>
#include <filesystem>
#include <vector>
#include <array>

#include <archive.h>
#include <archive_entry.h>

class archive_exception : std::exception {


    // exception interface
public:
    archive_exception(const std::string& err)
        :error(err){

    }
    const char *what() const noexcept{
        return error.c_str();
    }
    std::string error;
};

class archive_data {

public:
    archive_data(archive* arc): m_archive(arc){}

    std::vector<char> read(int block_size = 1024) const;
    bool read_into_file(const std::filesystem::path& path) const;


private:
    archive* m_archive = nullptr;
};

struct entry{
    std::string path;
    archive_data data;
};


class archive_iterator
    : public std::iterator<std::forward_iterator_tag, entry>
{
    using iterator = archive_iterator;
    friend class archive_reader;
public:
    archive_iterator() = default;
    archive_iterator(archive* archive, archive_entry* entry)
        : m_archive(archive), m_entry(entry) {}
    ~archive_iterator() = default;

    iterator  operator++(int) {
        auto result = *this;

        auto status = archive_read_next_header(m_archive, &m_entry);
        if(status != ARCHIVE_OK){
            m_entry = nullptr;
        }
        return result;
    }

    iterator& operator++() {
        auto status = archive_read_next_header(m_archive, &m_entry);

        if(status != ARCHIVE_OK){
            m_entry = nullptr;
        }

        return *this;
    }

    value_type operator* () const                    {
        return entry{.path = archive_entry_pathname(m_entry),
                     .data = archive_data(m_archive)};

    }
    value_type operator-> () const                    {
        return entry{.path = archive_entry_pathname(m_entry),
                     .data = archive_data(m_archive)};

    }

    iterator  operator+ (difference_type v)   const {
        archive_entry* entry = nullptr;
        for(auto i = 0;i<v;i++){
            auto status = archive_read_next_header(m_archive, &entry);

            if(status!=ARCHIVE_OK){
                return archive_iterator{m_archive, nullptr};
            }

            archive_read_data_skip(m_archive);
        }

        return {m_archive, entry};
    }

    bool      operator==(const iterator& rhs) const {
        return this->m_archive==rhs.m_archive
               && this->m_entry == rhs.m_entry;
    }

    bool      operator!=(const iterator& rhs) const {
        return !this->operator==(rhs);
    }
private:
    archive* m_archive = nullptr;
    archive_entry* m_entry = nullptr;
};

class archive_reader
{
public:
    archive_reader(const std::filesystem::path& file);
    archive_reader(void* data, int size);

    archive_iterator begin(){
        archive_iterator result;

        result.m_archive = m_archive.get();
        result++;

        return result;

    }
    archive_iterator end() const {return {m_archive.get(), nullptr};}
private:
    std::unique_ptr<struct archive, decltype (&archive_free)> m_archive
        {archive_read_new(), archive_free};
};
