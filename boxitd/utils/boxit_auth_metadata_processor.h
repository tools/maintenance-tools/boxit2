#pragma once
#include <grpc++/server.h>

class boxit_auth_metadata_processor : public grpc::AuthMetadataProcessor {


    // AuthMetadataProcessor interface
public:
    virtual grpc::Status Process(const InputMetadata &auth_metadata,
                                 grpc::AuthContext *context, OutputMetadata *consumed_auth_metadata,
                                 OutputMetadata *response_metadata) override;
};
