#pragma once

#include <boost/asio/ssl.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/http.hpp>

#include <filesystem>
#include <string>
#include <vector>

class https_downloader
{
public:
    struct endpoint {
        std::string host;
        int port;
        std::string target;
        std::string string() const;
    };

    https_downloader(struct endpoint ep, const std::string& user_agent = "BoxIt2");

    void download(const std::string& req_path,
                  const std::filesystem::path& path,
                  boost::system::error_code& ec);
    std::vector<uint8_t> download(const std::string& req_path,
                                  boost::system::error_code& ec);


    int get_retry_count() const;
    void set_retry_count(int new_retry_count);

    endpoint endpoint() const;
    void set_endpoint(const struct endpoint&);

private:


    boost::beast::ssl_stream<boost::beast::tcp_stream> setup_stream(boost::asio::io_context& io_ctx);
    boost::beast::http::request<boost::beast::http::string_body> setup_request(const std::string& req_path) const;
    int m_retry_count = 5;

    struct endpoint m_ep;
    std::string m_user_agent;

    boost::asio::ssl::context m_ctx;
};
