#include <boost/asio.hpp>
#include "boost/log/trivial.hpp"
#include <iostream>
#include "global.h"
#include <filesystem>
#include <utils/fs_watcher.h>
#include <utils.h>
#include <services/user_service.h>
#include <services/permissions_service.h>

#if NO_SSL
#include "server.h"
#else
#include "ssl_server.h"
#endif

int main()
{

    try
    {
        boost::asio::io_context io_context;

#if NO_SSL
        server server(global::settings.get<int>("port").value_or(5000));
#else
        ssl_server server(global::settings.get<int>("port").value_or(5000));
#endif
        server.start();
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << e.what();
        return 1;
    }

    return 0;
}
