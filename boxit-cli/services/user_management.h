#pragma once
#include <string>
#include <protos/user.grpc.pb.h>
#include "service_base.h"
class user_management: public service_base<UserController>
{
public:
    user_management(std::shared_ptr<grpc::ChannelInterface> channel);

    bool add_user(const std::string& name,const std::string& password);
    bool remove_user(const std::string& name);
};
