#pragma once
#include <string>
#include <map>
#include <set>
#include "service_base.h"
#include <protos/package.grpc.pb.h>
#include <types.h>
#include <optional>

class package_management : public service_base<PackageController>
{
public:
    package_management(std::shared_ptr<grpc::ChannelInterface> channelIf)
    :service_base<PackageController>(channelIf){}

    bool sync(const std::string &repo);
    bool snap(const std::string &source_branch, const std::string &target_branch);
    std::optional<branch_comparison_t> compare(const branch_t& source_branch, const branch_t& target_branch) const;

};
