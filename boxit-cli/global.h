#pragma once
#include <settings_manager.h>
#include <string>
#include <filesystem>
#include <utils.h>
#include "utils/credentials_manager.h"

#define STRING(s) STRING1(s)
#define STRING1(s) #s

namespace global {

namespace default_settings {
namespace fs = std::filesystem;

const inline fs::path share_prefix_path =  fs::path(STRING(INSTALL_PREFIX)) / "share/boxit2";
const inline fs::path certificate_path = share_prefix_path / "cert";
const inline fs::path config_path =  utils::standard_paths::config() / "boxit2";
const int default_port = 5000;

}

inline settings_manager settings(default_settings::config_path / "config.yml");
inline credentials_manager_cli credentials_manager(settings);
inline std::string current_profile = "default";

}
