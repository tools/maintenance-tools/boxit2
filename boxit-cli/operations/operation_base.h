#pragma once
#include <utils/cli_io.h>

#include <vector>
#include <map>
#include <string>
#include <variant>

#include <grpc++/channel.h>

class operation_executor;
namespace operations {

class operation_base {
    friend class ::operation_executor;
public:
    operation_base() = default;
    virtual ~operation_base() = default;

    virtual int operator()() const = 0;

protected:

    bool check_parameter_number(uint64_t size) const
    {
        if(m_positional_args.size() < size){
            cli_io::err("Not enough parameters");
            return false;
        }
        if(m_positional_args.size() > size){
            cli_io::err("Too much parameters");
            return false;
        }
        return true;
    }
    std::vector<std::string> m_positional_args;
    std::map<std::string, std::string> m_args;

    std::shared_ptr<grpc::Channel> m_channel;
};

}
