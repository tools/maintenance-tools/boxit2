#pragma once

#include "operation_base.h"
#include <services/package_management.h>
#include <boost/process.hpp>

namespace operations::repo {

class sync: public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{

        if(!package_management(m_channel).sync(m_positional_args.empty()?"":m_positional_args[0])){
            return 1;
        }
        cli_io::info("Successfully started synchronization");
        return 0;
    }
};

class snap: public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{
        if(!check_parameter_number(2)){
            return 1;
        }
        if(!package_management(m_channel).snap(m_positional_args[0], m_positional_args[1])){
            return 1;
        }
        cli_io::info(fmt::format("Successfully started snapshot from {} to {}",m_positional_args[0], m_positional_args[1]));
        return 0;
    }
};

class mount: public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{
        if(!check_parameter_number(1)){
            return 1;
        }
        namespace bp = boost::process;
        auto mount = bp::child("./boxit-fuse-app", m_positional_args[0], bp::std_out > bp::null );
        mount.join();
        if( mount.exit_code() != 0 ){
            cli_io::err("Failed to mount");
            return 1;
        }
        cli_io::info("Successfully mounted");
        return 0;
    }
};
class compare: public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{
        if(!check_parameter_number(2)){
            return 1;
        }

        auto comparison = package_management(m_channel).compare(m_positional_args[0], m_positional_args[1]);
        if(!comparison){
            return 1;
        }
        if(comparison->empty()){
            cli_io::info("Repositories are equal");
            return 0;
        }
        std::cout <<fmt::format("┌{0:─^30}┬{0:─^30}┬{0:─^30}┐\n","");
        std::cout <<fmt::format("│{:^30}│{:^30}│{:^30}│\n", "Package",m_positional_args[0], m_positional_args[1]);
        std::cout <<fmt::format("├{0:─^30}┼{0:─^30}┼{0:─^30}┤\n","");


        for(const auto& [name, evrs] : *comparison){
            auto first_evr = evrs.first;
            auto second_evr = evrs.second;
            if(first_evr.string()!=second_evr.string()){
                std::cout << fmt::format("│{:^30}│{:^30}│{:^30}│\n", name, first_evr.string(), second_evr.string());
            }
        }

        std::cout <<fmt::format("└{0:─^30}┴{0:─^30}┴{0:─^30}┘\n","");
        return 0;
    }

};


}
