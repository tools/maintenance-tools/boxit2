#pragma once

#include "operation_base.h"

#include <services/user_management.h>
#include <utils.h>

namespace operations::users {

class useradd : public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{
        if(!check_parameter_number(2)){
            return 1;
        }
        if(!user_management(m_channel).add_user(m_positional_args[0], utils::sha256(m_positional_args[1]))){
            cli_io::err("User add failed");
            return 1;
        }

        cli_io::info("Successfully added new user");
        return 0;
    }
};


class userdel : public operation_base {


    // operation_base interface
public:
    virtual int operator ()()  const override{
        if(!check_parameter_number(1)){
            return 1;
        }
        if(!user_management(m_channel).remove_user(m_positional_args[0])){
            cli_io::err("User remove failed");
            return 1;
        }
        cli_io::info(fmt::format("Successfully removed user {}", m_positional_args[0]));
        return 0;
    }
};

}
