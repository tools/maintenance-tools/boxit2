#pragma once
#include <chrono>
#include <functional>
#include <atomic>
#include <thread>

class timer 
{
public:
    
    bool start(const std::chrono::duration<double>& timeout){
        if(m_running) return false;
        
        m_begin = std::chrono::steady_clock::now();
        m_end = m_begin + timeout;
        
        m_running = true;
        std::thread t(m_main_loop);
        t.detach();
        
        return true;
    }
    bool stop(){
        if(!m_running) return false;
        
        m_running = false;
        
        return true;
    }
    
    void set_callback(const std::function<void()>& callback){
        m_callback = callback;
    }
    
private:
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>> m_begin,
                                                        m_end;
    std::atomic<bool> m_running;
    std::function<void()> m_callback = [](){};
    std::function<void()> m_main_loop = [this](){
        using namespace std::chrono_literals;
        
        while(true){
            if(!m_running || m_end<=std::chrono::steady_clock::now()) break;
            std::this_thread::sleep_for(1ms);
            if(!m_running || m_end<=std::chrono::steady_clock::now()) break;
        }
        
        if(m_running) {
            m_callback();
            m_running = false;
        }
    };
};
