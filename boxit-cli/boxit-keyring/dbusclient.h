/*
 *  BoxIt - Manjaro Linux Repository Management Software
 *  Roland Singer <roland@manjaro.org>
 *
 *  Copyright (C) 2007 Manjaro Linux
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DBUSCLIENT_H
#define DBUSCLIENT_H

#include "keyring_service.h"
#include <string>
#include <simppl/stub.h>
#include <utils/credentials.h>
#include <simppl/struct.h>
#include <simppl/string.h>
#include <simppl/dispatcher.h>


class DBusClient
{
public:
    explicit DBusClient(simppl::dbus::Dispatcher& disp):m_stub(disp, "keyring"){
        
    }
    
    std::optional<credentials> credentials();
    

    bool set_credentials(const struct credentials& creds);
private:
    simppl::dbus::Stub<service::BoxItKeyringService> m_stub;
};

#endif // DBUSCLIENT_H
