#pragma once
#include <simppl/interface.h>
#include <simppl/vector.h>

namespace service {
using namespace simppl::dbus;

INTERFACE(BoxItKeyringService)
{
    Property<std::string> host;
    Property<std::string> username;
    Property<std::vector<uint8_t>> password;
    
    Method<in<std::string>, in<std::string>, in<std::vector<uint8_t>>,out<bool>> set_credentials;
    
    
    
    BoxItKeyringService()
    : INIT(host),
    INIT(username),
    INIT(password),
    INIT(set_credentials)
    {
    }
};
}

