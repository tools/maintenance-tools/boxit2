/*
 *  BoxIt - Manjaro Linux Repository Management Software
 *  Roland Singer <roland@manjaro.org>
 *
 *  Copyright (C) 2007 Manjaro Linux
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <dbusserver.h>
#include <dbusclient.h>
#include <iostream>

bool check_running(){
    simppl::dbus::Dispatcher dsp("bus:session");
    
    DBusClient client(dsp);
    try{
        client.credentials();
    } catch (...) {
        return false;
    }
    return true;
    
    
}

int main(int argc, char *argv[])
{
    if(check_running()){
        return 0;
    }
    simppl::dbus::Dispatcher dsp("bus:session");
    
    DBusServer server(dsp);
    
    dsp.run();

}
