/*
 *  BoxIt - Manjaro Linux Repository Management Software
 *  Roland Singer <roland@manjaro.org>
 *
 *  Copyright (C) 2007 Manjaro Linux
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DBUSSERVER_H
#define DBUSSERVER_H

#include <keyring_service.h>

#include <string>
#include <mutex>
#include <chrono>
#include <iostream>
#include <simple_timer.h>
#include <simppl/skeleton.h>
#include <simppl/interface.h>
#include <simppl/struct.h>
#include <simppl/string.h>

#include <unistd.h>

class DBusServer : simppl::dbus::Skeleton<service::BoxItKeyringService>
{

public:
    DBusServer(simppl::dbus::Dispatcher& disp):simppl::dbus::Skeleton<service::BoxItKeyringService>(disp, "keyring")
    {
        using namespace std::chrono_literals;

        timer.set_callback([this](){
            _exit(0);
        });
        
        this->host = "";
        this->username = "";
        this->password = {};
        
        set_credentials >> [this](const std::string & host,const std::string& username, const std::vector<uint8_t>& salted_password){
            std::cout<<"set creds\n";
            std::cout.flush();
            timer.stop();
            
            this->host = host;
            this->username = username;
            this->password = salted_password;
            
            
            
            
            timer.start(2min);
            
            respond_with(set_credentials(true));
        };
        timer.start(2min);
    }

private:
    timer timer;
    std::mutex mutex;

    void timeout();

};

#endif // DBUSSERVER_H
