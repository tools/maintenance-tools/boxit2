#pragma once
#include <utils/credentials_manager.h>
#include <string>
class credentials_storage_keyring :  credentials_manager::storage_base
{
public:
    credentials_storage_keyring();

    virtual bool clear(const std::string& profile_name) = 0;


    virtual std::optional< std::string > load(const std::string& profile_name) = 0;


    virtual bool store(const std::string& password, const std::string& profile_name) = 0;

};


