/*
 *  BoxIt - Manjaro Linux Repository Management Software
 *  Roland Singer <roland@manjaro.org>
 *
 *  Copyright (C) 2007 Manjaro Linux
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dbusclient.h"
#include <utils.h>


bool DBusClient::set_credentials(const struct credentials& creds)
{
    return m_stub.set_credentials(creds.host, creds.username, utils::encrypt(creds.password.value_or("")));
}
std::optional<struct credentials> DBusClient::credentials()
{
    auto host = m_stub.host.get();
    auto username = m_stub.username.get();
    auto pwd = m_stub.password.get();
    
    if(host.empty() || username.empty() || pwd.empty()){
        return {};
    }
    struct credentials crd {host,username,utils::decrypt(pwd)};
    
    return crd;
}

