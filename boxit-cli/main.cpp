#include "global.h"
#include <services/user_management.h>
#include <services/package_management.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <optional>
#include <functional>
#include <fstream>
#include <boost/process.hpp>
#include <utils.h>
#include <unistd.h>
#include <fmt/printf.h>
#include <fmt/color.h>
#include <grpc++/grpc++.h>

#include <operation_executor.h>

#include <operations/repo.h>
#include <operations/users.h>

#include <cxxopts.hpp>

#include <utils/cli_io.h>



std::string prompt_to_config(const std::string& key, const std::string& description, bool allow_empty = false){
    auto value = global::settings.get<std::string>(key);
    if(!value){
        
        cli_io::question(description, allow_empty);

        global::settings.set(key, std::string(*value));

        global::settings.save();
        
    }
    return *value;
}

void passwd(){
    global::credentials_manager.reset(global::current_profile);
    global::credentials_manager.profile_prompt(global::current_profile);
}

void boxit_grpc_logger(gpr_log_func_args* args) {
    switch(args->severity){
    case GPR_LOG_SEVERITY_ERROR:
        cli_io::err(args->message);
        break;
    case GPR_LOG_SEVERITY_DEBUG:
    case GPR_LOG_SEVERITY_INFO:
        cli_io::info(args->message);
        break;
    }

}




void show_help(){
    fmt::print(fmt::emphasis::bold, utils::box);

    std::cout<<
        "Usage: boxit-cli OPERATION\n\n"
        "OPERATION:\n\n"
        "   sync [repo] - synchronize the database [for a specific repository]\n"
        "   compare [source] [target] - compare source branch and target branch\n"
#ifdef ENABLE_FS
        "   mount [folder] - mounts boxitfs as a local filesystem\n"
#endif
        "   snap [source] [target] - snapshot source branch to the target branch\n"
        "   useradd [name] - add a new user\n"
        "   userdel [name] - delete an existing user\n\n";
}
        

int main(int argc, char** argv){

    gpr_set_log_function(boxit_grpc_logger);
    operation_executor executor;
    global::credentials_manager.set_backend(new credentials_manager_storage_plain_config(global::settings));
    

    executor.add_operation<operations::repo::sync>("sync");
    executor.add_operation<operations::repo::compare>("compare");
    executor.add_operation<operations::repo::snap>("snap");
#ifdef ENABLE_FS
    executor.add_operation<operations::repo::mount>("mount");
#endif

    executor.add_operation<operations::users::useradd>("useradd");
    executor.add_operation<operations::users::userdel>("userdell");
    
    cxxopts::Options opts("BoxIt2 CLI");
    opts
        .positional_help("[optional args]")
        .show_positional_help();

    opts.add_options()
            ("positional",
                        "Action to send to BoxIt2 daemon",
                       cxxopts::value<std::vector<std::string>>());


    opts.parse_positional({"positional"});

    auto options = opts.parse(argc, argv);


    if(options.count("positional")==0){
        int retcode = 1;
        
        if(!global::credentials_manager.get()){
            passwd();
            retcode = 0;
        }
        auto creds = global::credentials_manager.get();
        
        cli_io::info(fmt::format("Profile info:"));
        cli_io::info(fmt::format("Hostname: {}", creds->host));
        cli_io::info(fmt::format("Username: {}", creds->username));
                
        show_help();
        return retcode;
    }
    auto operation = options["positional"].as<std::vector<std::string>>().front();

    
    global::credentials_manager.set_backend(new credentials_manager_storage_plain_config(global::settings));
    if(operation=="help"){
        show_help();
        return 0;
    }
    if(operation == "passwd"){
        passwd();
        return 0;
    }
    if(!executor.has_operation(operation)){
        cli_io::err("Invalid operation. Use help for the list of available operations");
        return 1;
    }
    executor.set_options(options);
    if(!executor.setup_grpc_channel()){
        return 1;
    }


    return executor.execute(operation);

}
