#pragma once
#include <string>
#include <optional>
struct credentials {
    std::string host;
    std::string username;
    std::optional<std::string> password;
};
