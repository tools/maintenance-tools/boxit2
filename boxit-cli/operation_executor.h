#pragma once

#include <operations/operation_base.h>
#include <global.h>

#include <memory>
#include <unordered_map>
#include <map>
#include <variant>
#include <fstream>

#include <grpc++/channel.h>
#include <grpc++/security/credentials.h>
#include <grpc++/create_channel.h>
#include <cxxopts.hpp>

class operation_executor
{
public:
    operation_executor()
    {    }

    int execute(const std::string& operation) const{
        return (*m_operations.at(operation))();
    }
    template <typename TOperation>
    void add_operation(const std::string& name){
        m_operations[name] = std::make_unique<TOperation>();
        inject_args(m_operations[name].get());
        m_operations[name]->m_channel = m_channel;
    }

    bool has_operation(const std::string& operation) const
    {
        return m_operations.contains(operation);
    }
    void set_options(const cxxopts::ParseResult& result){
        convert_args(result);

        std::for_each(m_operations.begin(), m_operations.end(), [this](const auto& operation){
            inject_args(operation.second.get());
        });
    }

    bool setup_grpc_channel(){
#if NO_SSL
        auto node = global::settings.get<YAML::Node>({"profiles", "default"});
        credentials creds;
        if(!node || !(*node)["host"].IsDefined()){
            auto creds = global::credentials_manager.profile_prompt();
        } else {
            creds.host = (*node)["host"].as<std::string>("");
        }
        auto insecure_creds = grpc::InsecureChannelCredentials();
        m_channel = grpc::CreateChannel(creds.host, insecure_creds);
#else
        auto cert = load_cert("server.crt");

        grpc::SslCredentialsOptions ssl_opts = {.pem_root_certs = cert};

        auto ssl_creds = grpc::SslCredentials(ssl_opts);
        grpc::ChannelArguments chargs;
        auto target_override = global::settings.get<std::string>("ssl-target");
        if(target_override.has_value()){
            chargs.SetSslTargetNameOverride(target_override.value());
        }
        auto creds = global::credentials_manager.profile_prompt();

        m_channel = grpc::CreateCustomChannel(creds.host,
                                              ssl_creds, chargs);
#endif
        std::for_each(m_operations.begin(), m_operations.end(), [this](const auto& operation){
            operation.second->m_channel = m_channel;
        });

        if(m_channel->GetState(true)==grpc_connectivity_state::GRPC_CHANNEL_SHUTDOWN
            || m_channel->GetState(true)==grpc_connectivity_state::GRPC_CHANNEL_TRANSIENT_FAILURE){
            cli_io::err(fmt::format("Server {} connection failed", creds.host));
            return false;
        }
        return true;
    }
private:
    inline std::string load_cert(const std::string& key_cert_file){
        auto path = global::settings.get<std::string>("cert-path").value_or(global::default_settings::certificate_path) + "/" + key_cert_file;
        if(!std::filesystem::exists(path)){
            return "";
        }

        std::ifstream cert_file(path);

        return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

    }



    void convert_args(const cxxopts::ParseResult& result){

        for(const auto& arg : result.arguments()){
            m_args.insert_or_assign(arg.key(),arg.value());
        }

        m_positional_args = result["positional"].as<std::vector<std::string>>();
        m_positional_args.erase(m_positional_args.begin());

    }

    void inject_args(operations::operation_base* operation){
        operation->m_args = m_args;
        operation->m_positional_args = m_positional_args;
    }

    std::unordered_map<std::string, std::unique_ptr<operations::operation_base>> m_operations;
    std::map<std::string, std::string> m_args;
    std::vector<std::string> m_positional_args;

    std::shared_ptr<grpc::Channel> m_channel;
};
