#include "fs_accessor.h"
#include <protos/filesystem.pb.h>
std::set<branch_t> fs_accessor::get_root()
{
    RootRequest rq;
    RootReply rp;
    auto ctx = create_default_context();
    
    auto status = m_stub->GetRoot(ctx.get(),rq,&rp);
    if(!status.ok()){
        return {};
    }
    
    return {rp.mutable_branches()->begin(),rp.mutable_branches()->end()};
}

std::set<repo_t> fs_accessor::get_branch(const branch_t &branch)
{
    BranchRequest rq;
    BranchReply rp;
    auto ctx = create_default_context();
    
    rq.set_name(branch);
    
    auto status = m_stub->GetBranch(ctx.get(), rq, &rp);
    
    if(!status.ok()){
        return {};
    }
    return {rp.mutable_repos()->begin(), rp.mutable_repos()->end()};
}

std::set<std::string> fs_accessor::get_repo_packages(const branch_t &branch, const repo_t &repo)
{
    PackagesRequest rq;
    PackagesReply rp;
    auto ctx = create_default_context();
    
    rq.set_branch(branch);
    rq.set_repo(repo);
    
    auto status = m_stub->GetRepoPackages(ctx.get(),rq,&rp);
    
    if(!status.ok()){
        return {};
    }
    return {rp.mutable_packages()->begin(), rp.mutable_packages()->end()};
}

bool fs_accessor::exists(const std::string& branch, const std::string& repo, const std::string& package)
{
    ExistsRequest rq;
    ExistsReply rp;
    
    auto ctx = create_default_context();
    
    rq.set_branch(branch);
    rq.set_repo(repo);
    rq.set_is_signature(package.ends_with(".sig")?true:false);
    rq.set_package_name(package);
    
    auto status = m_stub->Exists(ctx.get(), rq, &rp);
    
    return status.ok();
}
