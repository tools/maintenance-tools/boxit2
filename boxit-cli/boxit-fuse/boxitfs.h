#pragma once
#include <fuse3/fuse.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <grpc++/grpc++.h>

#include <string>
#include <filesystem>
#include <any>
#include <unordered_map>

#include <nodes/node_base.h>

class boxitfs
{
public:
    int getattr(const std::filesystem::path& path, struct stat& st, fuse_file_info &fi);
    int open(const std::filesystem::path& path, fuse_file_info &fi);
    int create(const std::filesystem::path& path,mode_t mode, fuse_file_info &fi);
    int readdir(const std::filesystem::path& path, std::any buffer, fuse_fill_dir_t filler, off_t offset, fuse_file_info &fi, fuse_readdir_flags flags);
    int write(const std::filesystem::path& path, std::any buffer, size_t size, off_t offset, fuse_file_info &fi);
    int unlink(const std::filesystem::path& path);
    int release(const std::filesystem::path& path, fuse_file_info &fi);
    int statfs(const std::filesystem::path& path, struct statvfs& sfs);
    int access(const std::filesystem::path& path, int mode);
    

    void set_сhannel(const std::shared_ptr<grpc::Channel> &channel);

private:
    std::shared_ptr<node_base> get_node_fi(const fuse_file_info& info);
    bool set_node_fi(fuse_file_info& info, const std::shared_ptr<node_base>& node);
    std::unordered_map<uint64_t, std::shared_ptr<node_base>> m_handles;
    std::shared_ptr<node_base> get_node(const std::filesystem::path& path);
    std::shared_ptr<grpc::Channel> m_channel;
};
