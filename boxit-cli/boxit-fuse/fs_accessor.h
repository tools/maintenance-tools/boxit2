#pragma once
#include <protos/filesystem.grpc.pb.h>
#include <types.h>
#include <package.h>
#include <services/service_base.h>
#include <utils.h>
class fs_accessor : public service_base<FileSystem>
{
public:
    fs_accessor(std::shared_ptr<grpc::ChannelInterface> channelIf)
        : service_base<FileSystem>(channelIf)
    {}

    std::set<branch_t> get_root();
    std::set<repo_t> get_branch(const branch_t& branch);
    std::set<std::string> get_repo_packages(const branch_t& branch, const repo_t& repo);
    bool exists(const std::string& branch = "", const std::string& repo = "", const std::string& package = "");

    uint64_t write_overlay(const std::string& filename, off_t offset, std::istream& data){
        WriteReply repl;
        auto ctx = create_default_context();
        auto writer = m_stub->WriteOverlay(ctx.get(), &repl);
        WriteRequest rq;

        auto md = rq.mutable_metadata();
        md->set_name(filename);
        md->set_offset(offset);


        writer->Write(rq);
        uint64_t total = 0;


        while(data){
            WriteRequest rq1;
            auto block = rq1.mutable_data_block();

            char buffer[1024];
            int n = 1024;
            if(!data.read(buffer, 1024)){
                n = data.gcount();
            }

            block->set_data(buffer,n);
            block->set_size(n);

            total+=n;


            writer->Write(rq1);

        }


        writer->WritesDone();
        auto status = writer->Finish();

        return total;
    }
    
    

    bool remove(const branch_t& branch,const repo_t& repo, const package& pkg){

        RemoveRequest rq;

        rq.set_branch(branch);
        rq.set_repo(repo);
        rq.set_package_name(pkg.filename());

        RemoveReply rp;

        auto ctx = create_default_context();

        auto status = m_stub->RemovePackage(ctx.get(), rq, &rp);


        return status.ok();
    }
    
    bool register_package(const branch_t& branch,const repo_t& repo, const package& pkg){
        RegisterRequest rq;
        RegisterReply rp;
        
        rq.set_branch(branch);
        rq.set_repo(repo);
        rq.set_package_name(pkg.filename());
        
        auto ctx = create_default_context();
        
        auto status = m_stub->RegisterPackage(ctx.get(), rq, &rp);
        
        return status.ok();
        
    }
    
};
