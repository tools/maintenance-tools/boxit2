#pragma once
#include "node_base.h"

class node_root : public node_base_folder
{
public:
    node_root() = default;
    virtual ~node_root() = default;
    fuse_result<struct stat>  getattr() override;
    virtual fuse_result<dirs_t> readdir() override;
    int access(int type) override;
};
