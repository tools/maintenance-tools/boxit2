#pragma once
#include "node_base.h"
#include <types.h>

class node_branch : public node_base_folder
{
public:
    explicit node_branch(const branch_t& branch_name):m_name(branch_name){}

    virtual fuse_result<dirs_t> readdir() override;
    virtual int access(int type) override;
private:
    branch_t m_name;
};
