#pragma once
#include "node_base.h"
#include <string>
#include <types.h>
class node_signature : public node_base_file
{
public:
     explicit node_signature(const branch_t& branch_name, const repo_t& repo_name, const std::string& name):node_base_file(), m_branch(branch_name),m_repo(repo_name), m_name(name){}
public:
    virtual int write(std::istream& data, off_t offset) override;

    virtual int unlink() override;
    
    virtual int release() override;

    int access(int type) override;
private:
    uint64_t bytes_written = 0;
    branch_t m_branch;
    repo_t m_repo;
    std::string m_name;
};
