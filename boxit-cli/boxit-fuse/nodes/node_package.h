#pragma once
#include "node_base.h"
#include <types.h>
class node_package : public node_base_file
{
public:
 explicit node_package(const branch_t& branch_name, const repo_t& repo_name, const package& pkg):node_base_file(), m_branch(branch_name),m_repo(repo_name), m_pkg(pkg){}
    virtual fuse_result<struct stat>  getattr() override;

    virtual int write(std::istream& data, off_t offset) override;

    virtual int unlink() override;
    
    virtual int release() override;

    int access(int mode) override;;

private:
    uint64_t bytes_written = 0;

    branch_t m_branch;
    repo_t m_repo;
    package m_pkg;
};
