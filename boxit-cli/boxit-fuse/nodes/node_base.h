#pragma once
#include <grpc++/grpc++.h>
#include <any>
#include <optional>
#include <set>
#include <istream>
#include <sys/stat.h>
#include <unistd.h>
#include <utils.h>
#include <expected.hpp>

using dirs_t = std::set<std::string>;

template<typename T>
using fuse_result = tl::expected<T, int>;

class node_base
{
public:
    virtual ~node_base() = default;
    virtual fuse_result<struct stat> getattr() {

        if(access(F_OK) == -ENOENT){
            return tl::make_unexpected(-ENOENT);
        }

        struct stat result;
   
        result.st_uid = getuid();
        result.st_gid = getgid();
        result.st_atim = result.st_ctim = utils::timepointToTimespec(std::chrono::system_clock::now());

        result.st_mode = S_IRUSR + S_IRGRP + S_IXUSR + S_IXGRP + S_IROTH;
        
        return result;
    };
//     virtual tl::expected<std::any, int> read(){return {{},-ENOENT};}
    virtual fuse_result<dirs_t> readdir(){return {};};
    virtual int write(std::istream& data, off_t offset){return -EPERM;}
    virtual int unlink(){return -EPERM;}
    virtual int release(){return -EPERM;}
    virtual int access(int type){ if(type==F_OK){ return -ENOENT;}; return 0;}

    void set_channel(std::shared_ptr<grpc::Channel> channel){
        m_channel = channel;
    }
    
protected:
    std::shared_ptr<grpc::Channel> m_channel;
};

class node_base_folder : public node_base 
{
public:
    virtual ~node_base_folder() = default;
    virtual fuse_result<struct stat> getattr() override{
        auto result = node_base::getattr();
        if(!result){return result;}
        
        result->st_mode += S_IFDIR;

        return result;
    }  
protected:
    node_base_folder():node_base(){}
};

class node_base_file : public node_base
{
public:
    virtual ~node_base_file() = default;

     virtual fuse_result<struct stat> getattr() override{
        auto result = node_base::getattr();
        if(!result){return result;}
        
        result->st_size = 0;
        
        result->st_mode += S_IFREG;

        return result;
    }  

};
