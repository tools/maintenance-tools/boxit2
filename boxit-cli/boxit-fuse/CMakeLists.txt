cmake_minimum_required(VERSION 3.5)

project(boxit-fuse)

find_package(Boost 1.66.0 COMPONENTS date_time filesystem system thread program_options)
find_package(PkgConfig REQUIRED)
find_package(Protobuf REQUIRED)
find_package(OpenSSL REQUIRED)

pkg_check_modules(YAML_CPP REQUIRED yaml-cpp)
pkg_check_modules(FUSE REQUIRED fuse3)
pkg_check_modules(SECRET REQUIRED libsecret-1 gio-2.0)

add_definitions(-D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=30)

add_executable(${PROJECT_NAME}
    nodes/node_base.cpp
    nodes/node_base.h
    nodes/node_root.cpp
    nodes/node_root.h
    nodes/node_package.cpp
    nodes/node_package.h
    nodes/node_branch.cpp
    nodes/node_branch.h
    nodes/node_repo.cpp
    nodes/node_repo.h
    nodes/node_package.cpp
    nodes/node_package.h
    nodes/node_signature.cpp
    nodes/node_signature.h
    ../utils/secret.cpp
    ../utils/secret.h
    ../utils/credentials_manager.cpp
    ../utils/credentials_manager.h
    ../utils/cli_io.cpp
    ../utils/cli_io.h
    ../services/package_management.cpp
    ../services/package_management.h
    ../services/client.cpp
    ../services/client.h
    ../services/service_base.cpp
    ../services/service_base.h
    boxitfs_fuse_operations.cpp
    boxitfs_fuse_operations.h
    boxitfs.cpp
    boxitfs.h
    fs_accessor.cpp
    fs_accessor.h
    main.cpp
    global.h
    )
    
include_directories(./)

target_include_directories(${PROJECT_NAME} PRIVATE ${SECRET_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME}
    libboxit
    Boost::date_time 
    Boost::filesystem 
    Boost::system 
    Boost::thread
    Threads::Threads
    OpenSSL::SSL
    OpenSSL::Crypto
    ${FUSE_LIBRARIES}
    ${YAML_CPP_LIBRARIES}
    ${SECRET_LIBRARIES}
    )
install(TARGETS ${PROJECT_NAME})
