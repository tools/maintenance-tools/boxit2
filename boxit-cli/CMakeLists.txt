cmake_minimum_required(VERSION 3.5)
 
project(boxit-cli)

find_package(Boost 1.66.0 COMPONENTS date_time filesystem system thread program_options)
find_package(PkgConfig REQUIRED)
find_package(Protobuf REQUIRED)
find_package(gRPC REQUIRED)
find_package(fmt REQUIRED)
find_package(OpenSSL REQUIRED)


pkg_check_modules(YAML_CPP REQUIRED yaml-cpp)
pkg_check_modules(SECRET REQUIRED libsecret-1 gio-2.0)


add_executable(${PROJECT_NAME}
    main.cpp
    ./global.cpp
    ./services/service_base.cpp
    ./services/user_management.cpp
    ./services/package_management.cpp
    #./boxit-keyring/dbusclient.cpp
    ./global.h
    ./services/service_base.h
    ./services/user_management.h
    ./services/package_management.h
    ./utils/secret.cpp
    ./utils/secret.h
    ./utils/credentials.h
    ./utils/credentials_manager.cpp
    ./utils/credentials_manager.h
    #./boxit-keyring/dbusclient.h
    ./utils/cli_io.cpp
    ./utils/cli_io.h
    ./operations/repo.h
    ./operations/users.h
    ./operations/operation_base.h
    ./operation_executor.h
    ./operation_executor.cpp
    )

include_directories(./)

include_directories(${CMAKE_SOURCE_DIR}/thirdparty/tl)
target_include_directories(${PROJECT_NAME} PRIVATE ${SECRET_INCLUDE_DIRS})
target_compile_definitions(${PROJECT_NAME} PRIVATE INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX})
target_link_libraries(${PROJECT_NAME}
    libboxit
    fmt::fmt
    Boost::date_time 
    Boost::filesystem 
    Boost::system 
    Boost::thread
    Threads::Threads
    OpenSSL::SSL
    OpenSSL::Crypto
    gRPC::grpc
    gRPC::grpc++
    ${YAML_CPP_LIBRARIES}
    ${SECRET_LIBRARIES}
    )

if (ENABLE_FS)
add_subdirectory(boxit-fuse)
target_compile_definitions(${PROJECT_NAME} PUBLIC ENABLE_FS)    
endif()

if (NO_SSL)
    target_compile_definitions(${PROJECT_NAME} PUBLIC NO_SSL)
endif()


#add_subdirectory(boxit-keyring)

install(TARGETS ${PROJECT_NAME})
