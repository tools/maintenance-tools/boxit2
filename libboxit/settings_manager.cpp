#include "settings_manager.h"
#include <fstream>

settings_manager::settings_manager(const std::filesystem::path &path)
    :m_path(path)
{
    try{
        m_settings_node = YAML::LoadFile(path);
    } catch (YAML::BadFile& exception){
        std::filesystem::create_directories(path.parent_path());
        std::ofstream creator(path);
        creator.close();
        m_settings_node = YAML::LoadFile(path);
    }
}

void settings_manager::reload()
{
    try{
        m_settings_node = YAML::LoadFile(m_path);
    } catch (YAML::BadFile& exception){
        std::filesystem::create_directories(m_path.parent_path());
        std::ofstream creator(m_path);
        creator.close();
        m_settings_node = YAML::LoadFile(m_path);
    }
}

void settings_manager::save() const
{
    std::filesystem::create_directories(m_path.parent_path());
    std::ofstream fout(m_path);
    fout<<m_settings_node;
    fout.flush();
}
