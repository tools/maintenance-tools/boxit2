#pragma once
#include <string>
#include <unordered_map>
#include <package.h>

enum package_arch {
    x86_64,
    aarch64,
};

using branch_t = std::string;
using repo_t = std::string;

using branch_comparison_t = std::unordered_map<std::string, std::pair<evr,evr>>;

