#ifndef BOX_TABLE_H
#define BOX_TABLE_H

#include <boost/functional/hash.hpp>
#include <set>
#include <string>
#include <tuple>
#include <types.h>
#include <unordered_map>

namespace std {
using key_t = std::tuple<arch_t, branch_t, repo_t>;

template<>
struct hash<key_t>
{
    std::size_t operator()(const key_t &k) const
    {
        std::size_t seed = 0;

        boost::hash_combine(seed, std::get<0>(k));
        boost::hash_combine(seed, std::get<1>(k));
        boost::hash_combine(seed, std::get<2>(k));

        return seed;
    }
};
} // namespace std

template<typename TArch, typename TBranch, typename TRepo, typename TValue>
class box_table : public std::unordered_map<std::tuple<TArch, TBranch, TRepo>, TValue>
{
public:
    box_table(){};

    std::set<TArch> aarchs() const { return m_aarches; }
    std::set<TBranch> branches() const { return m_branches; }
    std::set<TRepo> repos() const { return m_repos; }

    TValue operator()(const TArch &arch, const TBranch &branch, const TRepo &repo) const
    {
        return this->at({arch, branch, repo});
    }

    void emplace(const TArch &aarch, const TBranch &branch, const TRepo &repo, const TValue &value)
    {
        m_aarches.insert(aarch);
        m_branches.insert(branch);
        m_repos.insert(repo);

        this->insert({{aarch, branch, repo}, value});
    }

private:
    using std::unordered_map<std::tuple<TArch, TBranch, TRepo>, TValue>::insert;
    using std::unordered_map<std::tuple<TArch, TBranch, TRepo>, TValue>::at;

    std::set<TArch> m_aarches;
    std::set<TBranch> m_branches;
    std::set<TRepo> m_repos;
};

#endif // BOX_TABLE_H
